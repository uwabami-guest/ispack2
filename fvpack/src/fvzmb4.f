************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
      SUBROUTINE FVZMB4(M,KHH,LS,LL,X,T)

      IMPLICIT REAL*8(A-H,O-Z)      
      DIMENSION T(2,0:*),X(M,2,0:*)

      IF(LS.EQ.0) THEN
        LSD=1
        DO J=0,KHH-1
          J00=J
          J01=J00+KHH
          J10=J00+2*KHH
          J11=J00+3*KHH
          DO IV=1,M
            TMP10R=X(IV,1,J00)-X(IV,1,J10)
            TMP10I=X(IV,2,J00)-X(IV,2,J10)
            TMP00R=2D0*X(IV,1,J00)-TMP10R
            TMP00I=2D0*X(IV,2,J00)-TMP10I

            TMP11R=X(IV,1,J01)-X(IV,1,J11)
            TMP11I=X(IV,2,J01)-X(IV,2,J11)
            TMP01R=2D0*X(IV,1,J01)-TMP11R
            TMP01I=2D0*X(IV,2,J01)-TMP11I

            X(IV,1,J01)=TMP00R-TMP01R
            X(IV,2,J01)=TMP00I-TMP01I
            X(IV,1,J00)=2D0*TMP00R-X(IV,1,J01)
            X(IV,2,J00)=2D0*TMP00I-X(IV,2,J01)

            X(IV,1,J11)=TMP10R+TMP11I
            X(IV,2,J11)=TMP10I-TMP11R
            X(IV,1,J10)=2D0*TMP10R-X(IV,1,J11)
            X(IV,2,J10)=2D0*TMP10I-X(IV,2,J11)
          END DO
        END DO
      ELSE
        LSD=LS
      END IF

      DO I=LSD,LS+LL-1
        I0=I*KHH*4
        DO J=0,KHH-1
          J00=J+I0
          J01=J00+KHH
          J10=J00+2*KHH
          J11=J00+3*KHH
          DO IV=1,M
            TMP10R=X(IV,1,J00)-T(1,I)*X(IV,1,J10)+T(2,I)*X(IV,2,J10)
            TMP10I=X(IV,2,J00)-T(1,I)*X(IV,2,J10)-T(2,I)*X(IV,1,J10)
            TMP00R=2D0*X(IV,1,J00)-TMP10R
            TMP00I=2D0*X(IV,2,J00)-TMP10I

            TMP11R=X(IV,1,J01)-T(1,I)*X(IV,1,J11)+T(2,I)*X(IV,2,J11)
            TMP11I=X(IV,2,J01)-T(1,I)*X(IV,2,J11)-T(2,I)*X(IV,1,J11)
            TMP01R=2D0*X(IV,1,J01)-TMP11R
            TMP01I=2D0*X(IV,2,J01)-TMP11I

            X(IV,1,J01)=TMP00R-T(1,I*2)*TMP01R+T(2,I*2)*TMP01I
            X(IV,2,J01)=TMP00I-T(1,I*2)*TMP01I-T(2,I*2)*TMP01R
            X(IV,1,J00)=2D0*TMP00R-X(IV,1,J01)
            X(IV,2,J00)=2D0*TMP00I-X(IV,2,J01)

            X(IV,1,J11)=TMP10R+T(2,I*2)*TMP11R+T(1,I*2)*TMP11I
            X(IV,2,J11)=TMP10I+T(2,I*2)*TMP11I-T(1,I*2)*TMP11R
            X(IV,1,J10)=2D0*TMP10R-X(IV,1,J11)
            X(IV,2,J10)=2D0*TMP10I-X(IV,2,J11)
          END DO
        END DO
      END DO

      END
