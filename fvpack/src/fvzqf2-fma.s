########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl fvzqf2_
.globl _fvzqf2_	
fvzqf2_:
_fvzqf2_:	
	movl (%rdi), %edi  # NH が rdi に
      # X の先頭アドレスは rsi

	vbroadcastsd C2(%rip),%ymm11 # 倍精度不動小数点の 2 を ymm11 の4箇所に

#	shlq $6,%rdi # rdi に NH*64 が

	shlq $2,%rdi # rdi に NH*4 が	
        cvtsi2sd  %edi, %xmm0	

	movsd C2(%rip),%xmm1
	divsd %xmm0, %xmm1
	movsd %xmm1,-8(%rsp)	
	vbroadcastsd -8(%rsp),%ymm12 

	shlq $4,%rdi # rdi に NH*64 が
	
	movq %rdi,%rax
	addq %rsi,%rax
	
#.align 16
L1:
	vmovapd   (%rsi),      %ymm0 # 0R
	vmovapd 32(%rsi),      %ymm1 # 0I	
	vmovapd   (%rsi,%rdi), %ymm2 # 1R
	vmovapd 32(%rsi,%rdi), %ymm3 # 1I

#-- scaling --
#	vmulpd %ymm12,%ymm0,%ymm0
#	vmulpd %ymm12,%ymm1,%ymm1
	vmulpd %ymm12,%ymm2,%ymm2
	vmulpd %ymm12,%ymm3,%ymm3
#-------------	

#	vaddpd %ymm2,%ymm0,%ymm0
	vfmadd213pd %ymm2,%ymm12,%ymm0 # ymm0 = ymm0 * ymm12 + ymm2	
#	vaddpd %ymm3,%ymm1,%ymm1
	vfmadd213pd %ymm3,%ymm12,%ymm1 # ymm1 = ymm1 * ymm12 + ymm3
	
#	vmulpd %ymm11,%ymm2,%ymm2
#	vsubpd %ymm2,%ymm0,%ymm2
	vfnmadd213pd %ymm0,%ymm11,%ymm2 # ymm2 = - ymm2 * ymm11 + ymm0	
#	vmulpd %ymm11,%ymm3,%ymm3
#	vsubpd %ymm3,%ymm1,%ymm3
	vfnmadd213pd %ymm1,%ymm11,%ymm3 # ymm3 = - ymm3 * ymm11 + ymm1	

	vmovapd %ymm0,   (%rsi)
	vmovapd %ymm1, 32(%rsi)
	vmovapd %ymm2,   (%rsi,%rdi)
	vmovapd %ymm3, 32(%rsi,%rdi)
#-----
	addq $64,%rsi
	cmpq %rsi,%rax
	jne L1

	ret
       
C2: # 倍精度不動小数点の 2
	.long   0x00000000,0x40000000
	
