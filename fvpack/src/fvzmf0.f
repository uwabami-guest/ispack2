************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
      SUBROUTINE FVZMF0(M,NHH,X)

      IMPLICIT REAL*8(A-H,O-Z)      
      DIMENSION X(M,2,0:*)

      F=1D0/(4*NHH)

      DO J=0,NHH-1
        J00=J
        J01=J00+NHH
        J10=J00+2*NHH
        J11=J00+3*NHH
        DO IV=1,M
          TMP10R=F*X(IV,1,J00)-F*X(IV,1,J10)
          TMP10I=F*X(IV,2,J00)-F*X(IV,2,J10)
          TMP00R=2D0*F*X(IV,1,J00)-TMP10R
          TMP00I=2D0*F*X(IV,2,J00)-TMP10I

          TMP11R=F*X(IV,1,J01)-F*X(IV,1,J11)
          TMP11I=F*X(IV,2,J01)-F*X(IV,2,J11)
          TMP01R=2D0*F*X(IV,1,J01)-TMP11R
          TMP01I=2D0*F*X(IV,2,J01)-TMP11I

          X(IV,1,J01)=TMP00R-TMP01R
          X(IV,2,J01)=TMP00I-TMP01I
          X(IV,1,J00)=2D0*TMP00R-X(IV,1,J01)
          X(IV,2,J00)=2D0*TMP00I-X(IV,2,J01)

          X(IV,1,J11)=TMP10R-TMP11I
          X(IV,2,J11)=TMP10I+TMP11R
          X(IV,1,J10)=2D0*TMP10R-X(IV,1,J11)
          X(IV,2,J10)=2D0*TMP10I-X(IV,2,J11)
        END DO
      END DO

      END
