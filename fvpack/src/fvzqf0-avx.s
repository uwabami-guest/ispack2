########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl fvzqf0_
.globl _fvzqf0_	
fvzqf0_:
_fvzqf0_:	
	movl (%rdi), %edi  # NHH が rdi に
      # X の先頭アドレスは rsi

	vbroadcastsd C2(%rip),%ymm11 # 倍精度不動小数点の 2 を ymm11 の4箇所に

	shlq $3,%rdi # rdi に NHH*8 が	
        cvtsi2sd  %edi, %xmm0	

	movsd C2(%rip),%xmm1
	divsd %xmm0, %xmm1
	movsd %xmm1,-8(%rsp)	
	vbroadcastsd -8(%rsp),%ymm12 

	shlq $3,%rdi # rdi に NHH*64 が

# I=0 の場合	

	lea (%rdi,%rdi),%rcx
	lea (%rdi,%rcx),%rdx
	lea (%rdi,%rsi),%rax
#.align 16
L1:
	vmovapd  (%rsi),   %ymm0 # 00R
	vmovapd 32(%rsi),  %ymm1 # 00I	
	vmovapd  (%rsi,%rcx),  %ymm2 # 10R
	vmovapd 32(%rsi,%rcx),  %ymm3 # 10I
	vmovapd  (%rsi,%rdi),  %ymm4 # 01R
	vmovapd 32(%rsi,%rdi),  %ymm5 # 01I
	vmovapd  (%rsi,%rdx),  %ymm6 # 11R
	vmovapd 32(%rsi,%rdx),  %ymm7 # 11I

#-- scaling --
	vmulpd %ymm12,%ymm0,%ymm0
	vmulpd %ymm12,%ymm1,%ymm1
	vmulpd %ymm12,%ymm2,%ymm2
	vmulpd %ymm12,%ymm3,%ymm3
	vmulpd %ymm12,%ymm4,%ymm4
	vmulpd %ymm12,%ymm5,%ymm5
	vmulpd %ymm12,%ymm6,%ymm6
	vmulpd %ymm12,%ymm7,%ymm7
#-------------

	vsubpd %ymm2,%ymm0,%ymm2
	vsubpd %ymm3,%ymm1,%ymm3
	
	vmulpd %ymm11,%ymm0,%ymm0
	vsubpd %ymm2,%ymm0,%ymm0
	vmulpd %ymm11,%ymm1,%ymm1
	vsubpd %ymm3,%ymm1,%ymm1

#
	vsubpd %ymm6,%ymm4,%ymm6
	vsubpd %ymm7,%ymm5,%ymm7

	vmulpd %ymm11,%ymm4,%ymm4
	vsubpd %ymm6,%ymm4,%ymm4
	vmulpd %ymm11,%ymm5,%ymm5
	vsubpd %ymm7,%ymm5,%ymm5

#--
	vsubpd %ymm4,%ymm0,%ymm9
	vmovapd %ymm9,(%rsi,%rdi)  # 01R		
	vsubpd %ymm5,%ymm1,%ymm5
	vmovapd %ymm5,32(%rsi,%rdi)   # 01I	
	
	vmulpd %ymm11,%ymm0,%ymm0
	vsubpd %ymm9,%ymm0,%ymm0
	vmovapd %ymm0, (%rsi)       # 00R	
	vmulpd %ymm11,%ymm1,%ymm1
	vsubpd %ymm5,%ymm1,%ymm1
	vmovapd %ymm1,32(%rsi)       # 00I	
	
#	vaddpd %ymm7,%ymm2,%ymm9
	vsubpd %ymm7,%ymm2,%ymm9
	vmovapd %ymm9, (%rsi,%rdx)   # 11R		
	
#	vsubpd %ymm6,%ymm3,%ymm7
	vaddpd %ymm6,%ymm3,%ymm7	
	vmovapd %ymm7,32(%rsi,%rdx)   # 11I		

	vmulpd %ymm11,%ymm2,%ymm2
	vsubpd %ymm9,%ymm2,%ymm2
	vmovapd %ymm2, (%rsi,%rcx)   # 10R		
	vmulpd %ymm11,%ymm3,%ymm3
	vsubpd %ymm7,%ymm3,%ymm3
	vmovapd %ymm3,32(%rsi,%rcx)   # 10I	
	
#-----
	addq $64,%rsi
	cmpq %rsi,%rax
	jne L1

	ret
       
C2: # 倍精度不動小数点の 2
	.long   0x00000000,0x40000000
	
