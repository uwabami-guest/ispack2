************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
************************************************************************
      SUBROUTINE FVZTFA(M,N,X,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)      
      DIMENSION X(*),IT(*),T(*)

      IF(M.EQ.4) THEN
        CALL FVZQFA(N,X,IT,T)
      ELSE IF(M.EQ.8) THEN
        CALL FVZOFA(N,X,IT,T)
      ELSE
        CALL FVZMFA(M,N,X,IT,T)
      END IF

      END
************************************************************************
      SUBROUTINE FVZTBA(M,N,X,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)      
      DIMENSION X(*),IT(*),T(*)

      IF(M.EQ.4) THEN
        CALL FVZQBA(N,X,IT,T)
      ELSE IF(M.EQ.8) THEN
        CALL FVZOBA(N,X,IT,T)
      ELSE
        CALL FVZMBA(M,N,X,IT,T)
      END IF

      END
************************************************************************
      SUBROUTINE FVZMFA(M,N,X,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)      
      DIMENSION T(2,0:N/2-1),IT(2,0:N/2-1)
      DIMENSION X(M,2,0:N-1)

      ND=N
      L=1
      IF(IT(2,0).EQ.1) THEN
        CALL FVZMF2(M,N/2,X)
        ND=N/2
        L=2
      ELSE
        CALL FVZMF0(M,N/4,X)
        ND=N/4
        L=4
      END IF

      IF(ND.LT.256) THEN
        DO WHILE(ND.GT.1)
          ND=ND/4
          CALL FVZMF4(M,ND,0,L,X,T)        
          L=L*4
        END DO
      ELSE
        DO WHILE(ND.GT.256)
          ND=ND/4
          CALL FVZMF4(M,ND,0,L,X,T)        
          L=L*4
        END DO
        DO I=0,N/256-1
          NDD=ND
          LD=1
          DO WHILE(NDD.GT.1)
            NDD=NDD/4
            CALL FVZMF4(M,NDD,I*LD,LD,X,T)        
            LD=LD*4
          END DO
        END DO
      END IF

      CALL FVZMTP(M,X,IT)

      END
************************************************************************
      SUBROUTINE FVZMBA(M,N,X,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)      
      DIMENSION T(2,0:N/2-1),IT(2,0:N/2-1)
      DIMENSION X(M,2,0:N-1)

      ND=N
      L=1
      IF(IT(2,0).EQ.1) THEN
        CALL FVZMB2(M,N/2,X)
        ND=N/2
        L=2
      ELSE
        CALL FVZMB0(M,N/4,X)
        ND=N/4
        L=4
      END IF

      IF(ND.LT.256) THEN
        DO WHILE(ND.GT.1)
          ND=ND/4
          CALL FVZMB4(M,ND,0,L,X,T)        
          L=L*4
        END DO
      ELSE
        DO WHILE(ND.GT.256)
          ND=ND/4
          CALL FVZMB4(M,ND,0,L,X,T)        
          L=L*4
        END DO
        DO I=0,N/256-1
          NDD=ND
          LD=1
          DO WHILE(NDD.GT.1)
            NDD=NDD/4
            CALL FVZMB4(M,NDD,I*LD,LD,X,T)        
            LD=LD*4
          END DO
        END DO
      END IF

      CALL FVZMTP(M,X,IT)

      END
************************************************************************
      SUBROUTINE FVZQFA(N,X,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)      
      PARAMETER(M=4)
      DIMENSION T(2,0:N/2-1),IT(2,0:N/2-1)
      DIMENSION X(M,2,0:N-1)

      ND=N
      L=1
      IF(IT(2,0).EQ.1) THEN
        CALL FVZQF2(N/2,X)
        ND=N/2
        L=2
      ELSE
        CALL FVZQF0(N/4,X)
        ND=N/4
        L=4
      END IF

      IF(ND.LT.256) THEN
        DO WHILE(ND.GT.1)
          ND=ND/4
          CALL FVZQF4(ND,0,L,X,T)        
          L=L*4
        END DO
      ELSE
        DO WHILE(ND.GT.256)
          ND=ND/4
          CALL FVZQF4(ND,0,L,X,T)        
          L=L*4
        END DO
        DO I=0,N/256-1
          NDD=ND
          LD=1
          DO WHILE(NDD.GT.1)
            NDD=NDD/4
            CALL FVZQF4(NDD,I*LD,LD,X,T)        
            LD=LD*4
          END DO
        END DO
      END IF

      CALL FVZQTP(X,IT)

      END
************************************************************************
      SUBROUTINE FVZQBA(N,X,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)      
      PARAMETER(M=4)
      DIMENSION T(2,0:N/2-1),IT(2,0:N/2-1)
      DIMENSION X(M,2,0:N-1)

      ND=N
      L=1
      IF(IT(2,0).EQ.1) THEN
        CALL FVZQB2(N/2,X)
        ND=N/2
        L=2
      ELSE
        CALL FVZQB0(N/4,X)
        ND=N/4
        L=4
      END IF

      IF(ND.LT.256) THEN
        DO WHILE(ND.GT.1)
          ND=ND/4
          CALL FVZQB4(ND,0,L,X,T)        
          L=L*4
        END DO
      ELSE
        DO WHILE(ND.GT.256)
          ND=ND/4
          CALL FVZQB4(ND,0,L,X,T)        
          L=L*4
        END DO
        DO I=0,N/256-1
          NDD=ND
          LD=1
          DO WHILE(NDD.GT.1)
            NDD=NDD/4
            CALL FVZQB4(NDD,I*LD,LD,X,T)        
            LD=LD*4
          END DO
        END DO
      END IF

      CALL FVZQTP(X,IT)

      END
************************************************************************
      SUBROUTINE FVZOFA(N,X,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)      
      PARAMETER(M=8)
      DIMENSION T(2,0:N/2-1),IT(2,0:N/2-1)
      DIMENSION X(M,2,0:N-1)

      ND=N
      L=1
      IF(IT(2,0).EQ.1) THEN
        CALL FVZOF2(N/2,X)
        ND=N/2
        L=2
      ELSE
        CALL FVZOF0(N/4,X)
        ND=N/4
        L=4
      END IF

      IF(ND.LT.256) THEN
        DO WHILE(ND.GT.1)
          ND=ND/4
          CALL FVZOF4(ND,0,L,X,T)        
          L=L*4
        END DO
      ELSE
        DO WHILE(ND.GT.256)
          ND=ND/4
          CALL FVZOF4(ND,0,L,X,T)        
          L=L*4
        END DO
        DO I=0,N/256-1
          NDD=ND
          LD=1
          DO WHILE(NDD.GT.1)
            NDD=NDD/4
            CALL FVZOF4(NDD,I*LD,LD,X,T)        
            LD=LD*4
          END DO
        END DO
      END IF

      CALL FVZOTP(X,IT)

      END
************************************************************************
      SUBROUTINE FVZOBA(N,X,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)      
      PARAMETER(M=8)
      DIMENSION T(2,0:N/2-1),IT(2,0:N/2-1)
      DIMENSION X(M,2,0:N-1)

      ND=N
      L=1
      IF(IT(2,0).EQ.1) THEN
        CALL FVZOB2(N/2,X)
        ND=N/2
        L=2
      ELSE
        CALL FVZOB0(N/4,X)
        ND=N/4
        L=4
      END IF

      IF(ND.LT.256) THEN
        DO WHILE(ND.GT.1)
          ND=ND/4
          CALL FVZOB4(ND,0,L,X,T)        
          L=L*4
        END DO
      ELSE
        DO WHILE(ND.GT.256)
          ND=ND/4
          CALL FVZOB4(ND,0,L,X,T)        
          L=L*4
        END DO
        DO I=0,N/256-1
          NDD=ND
          LD=1
          DO WHILE(NDD.GT.1)
            NDD=NDD/4
            CALL FVZOB4(NDD,I*LD,LD,X,T)        
            LD=LD*4
          END DO
        END DO
      END IF

      CALL FVZOTP(X,IT)

      END
************************************************************************
      SUBROUTINE FVZINI(N,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION T(2,0:N/2-1),IT(2,0:N/2-1) 
      REAL*16 PI
!     IT(1,0) にはテーブルの個数を入れとく

      PI=4*ATAN(1Q0)

      IF(N.LT.2) THEN
        print *,'fvpack error: N.NE.2**P'
        STOP
      END IF

      JS=N
      DO WHILE(MOD(JS,4).EQ.0)
        JS=JS/4
      END DO
      
      IF(JS.GE.3) THEN
        print *,'fvpack error: N.NE.2**P'
        STOP
      END IF

      DO L=0,N/2-1
        IT(1,L)=0
        IT(2,L)=0
      END DO

      I=0
      L=0
      DO J=1,N-2
        K=N/2
        DO WHILE(K.LE.I)
          I=I-K
          K=K/2
        END DO
        I=I+K
        IF(J.LT.I) THEN
          L=L+1
          IT(1,L)=J
          IT(2,L)=I
        END IF
      END DO

      IT(1,0)=L

      IF(JS.EQ.1) THEN
        IT(2,0)=0 ! N=4**P
      ELSE IF(JS.EQ.2) THEN
        IT(2,0)=1 ! N=4**P*2
      END IF

      DO I=0,N/2-1
        T(1,I)=2D0
        T(2,I)=2D0
      END DO

      DO L=1,IT(1,0)
        J=IT(1,L)
        I=IT(2,L)
        IF(J.LE.N/2-1) THEN
          T(1,I/2)=COS(2*PI*J/N)
          T(2,I/2)=SIN(2*PI*J/N)        
        END IF
        IF(I.LE.N/2-1) THEN
          T(1,J/2)=COS(2*PI*I/N)
          T(2,J/2)=SIN(2*PI*I/N)        
        END IF
      END DO

      DO I=0,N/2-1,2
        IF(T(1,I/2).EQ.2D0) THEN
          T(1,I/2)=COS(2*PI*I/N)
          T(2,I/2)=SIN(2*PI*I/N)        
        END IF
      END DO

      END
