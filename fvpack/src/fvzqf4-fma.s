########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl fvzqf4_
.globl _fvzqf4_	
fvzqf4_:
_fvzqf4_:	
	pushq %rbx
	pushq %rbp	
	pushq %r12
	pushq %r13
	pushq %r14
	pushq %r15	
	
	movl (%rdi), %edi  # KHH が rdi に
	movl (%rsi), %esi # LS が rsi に
	movl (%rdx), %edx # LL が rdx に		
      # X の先頭アドレスは rcx
      # T の先頭アドレスは r8

	vbroadcastsd C2(%rip),%ymm11 # 倍精度不動小数点の 2 を ymm11 の4箇所に

	movq %rsi,%rax # rax に LS
	addq %rsi,%rdx # rdx に LS+LL (終了条件)

	shlq $6,%rdi # KHH*M*8*2=KHH*64
	movq %rsi,%r9 # r9 に LS	
	imulq %rdi,%r9 # r9 に LS*KHH*64
	shlq $2,%r9 # r9 に LS*KHH*64*4


# %rax を I のためのカウンタとする

L1:

# %rbp を J*16*4 のためのカウンタとする


	cmpq $0,%rax
	jne L19

# I=0 の場合	

	lea (%rdi,%r9),%r10
	lea (%rdi,%r10),%r11
	lea (%rdi,%r11),%r12	
	

	xorq %rbp,%rbp
	movq %rcx,%r13
#.align 16
L18:
	vmovapd  (%r13,%r9),   %ymm0 # 00R
	vmovapd 32(%r13,%r9),  %ymm1 # 00I	
	vmovapd  (%r13,%r11),  %ymm2 # 10R
	vmovapd 32(%r13,%r11),  %ymm3 # 10I
	vmovapd  (%r13,%r10),  %ymm4 # 01R
	vmovapd 32(%r13,%r10),  %ymm5 # 01I
	vmovapd  (%r13,%r12),  %ymm6 # 11R
	vmovapd 32(%r13,%r12),  %ymm7 # 11I

	vsubpd %ymm2,%ymm0,%ymm2
	vsubpd %ymm3,%ymm1,%ymm3
	
#	vmulpd %ymm11,%ymm0,%ymm0
#	vsubpd %ymm2,%ymm0,%ymm0
	vfmsub213pd %ymm2,%ymm11,%ymm0 # ymm0 = ymm0 * ymm11 - ymm2
#	vmulpd %ymm11,%ymm1,%ymm1
#	vsubpd %ymm3,%ymm1,%ymm1
	vfmsub213pd %ymm3,%ymm11,%ymm1 # ymm1 = ymm1 * ymm11 - ymm3	

	vsubpd %ymm6,%ymm4,%ymm6
	vsubpd %ymm7,%ymm5,%ymm7

#	vmulpd %ymm11,%ymm4,%ymm4
#	vsubpd %ymm6,%ymm4,%ymm4
	vfmsub213pd %ymm6,%ymm11,%ymm4 # ymm4 = ymm4 * ymm11 - ymm6	
	
#	vmulpd %ymm11,%ymm5,%ymm5
#	vsubpd %ymm7,%ymm5,%ymm5
	vfmsub213pd %ymm7,%ymm11,%ymm5 # ymm5 = ymm5 * ymm11 - ymm7	

#--
	vsubpd %ymm4,%ymm0,%ymm9
	vmovapd %ymm9,(%r13,%r10)  # 01R		
	vsubpd %ymm5,%ymm1,%ymm5
	vmovapd %ymm5,32(%r13,%r10)   # 01I	
	
#	vmulpd %ymm11,%ymm0,%ymm0
#	vsubpd %ymm9,%ymm0,%ymm0
	vfmsub213pd %ymm9,%ymm11,%ymm0 # ymm0 = ymm0 * ymm11 - ymm9	
	vmovapd %ymm0, (%r13,%r9)       # 00R	
#	vmulpd %ymm11,%ymm1,%ymm1
#	vsubpd %ymm5,%ymm1,%ymm1
	vfmsub213pd %ymm5,%ymm11,%ymm1 # ymm1 = ymm1 * ymm11 - ymm5	
	vmovapd %ymm1,32(%r13,%r9)       # 00I	
	
#	vaddpd %ymm7,%ymm2,%ymm9
	vsubpd %ymm7,%ymm2,%ymm9	
	vmovapd %ymm9, (%r13,%r12)   # 11R		
	
#	vsubpd %ymm6,%ymm3,%ymm7
	vaddpd %ymm6,%ymm3,%ymm7	
	vmovapd %ymm7,32(%r13,%r12)   # 11I		

#	vmulpd %ymm11,%ymm2,%ymm2
#	vsubpd %ymm9,%ymm2,%ymm2
	vfmsub213pd %ymm9,%ymm11,%ymm2 # ymm2 = ymm2 * ymm11 - ymm9	
	vmovapd %ymm2, (%r13,%r11)   # 10R		
#	vmulpd %ymm11,%ymm3,%ymm3
#	vsubpd %ymm7,%ymm3,%ymm3
	vfmsub213pd %ymm7,%ymm11,%ymm3 # ymm3 = ymm3 * ymm11 - ymm7	
	vmovapd %ymm3,32(%r13,%r11)   # 10I	
	
#-----
	addq $64,%r13
	addq $64,%rbp # J のカウンタ増やす	
	cmpq %rbp,%rdi
	jne L18

	lea (%rdi,%r12),%r9
	addq $1,%rax	
	cmpq %rax,%rdx
	je LE
	
#----------------------- I=0以外の場合 -----------	

L19:

	lea (%rdi,%r9),%r10
	lea (%rdi,%r10),%r11
	lea (%rdi,%r11),%r12	
	

	movq %rax,%rbx
	shlq $4,%rbx # KHI0 に対応するアドレス
	vbroadcastsd  (%r8,%rbx),%ymm12 # T(1,KHI0)
	vbroadcastsd 8(%r8,%rbx),%ymm13 # T(2,KHI0)
	shlq $1,%rbx # KHHI0 に対応するアドレス	
	vbroadcastsd  (%r8,%rbx),%ymm14 # T(1,KHHI0)
	vbroadcastsd 8(%r8,%rbx),%ymm15 # T(2,KHHI0)
	
#	xorq %rbp,%rbp
	movq %rcx,%r13
	movq %rcx,%rbp	
	addq %rdi,%rbp
#.align 16
L2:
	vmovapd  (%r13,%r9),   %ymm0 # 00R
	vmovapd 32(%r13,%r9),  %ymm1 # 00I	
	vmovapd  (%r13,%r11),  %ymm9 # 10R		
	vmovapd 32(%r13,%r11),  %ymm3 # 10I
	vmovapd  (%r13,%r10),  %ymm4 # 01R
	vmovapd 32(%r13,%r10),  %ymm5 # 01I
	vmovapd  (%r13,%r12),  %ymm10 # 11R
	vmovapd 32(%r13,%r12),  %ymm7 # 11I


#	vmulpd %ymm12,%ymm9,%ymm8		
#	vsubpd %ymm8,%ymm0,%ymm2
	vmovapd %ymm0,%ymm2		
	vfnmadd231pd %ymm9,%ymm12,%ymm2 # ymm2 = - ymm9 * ymm12 + ymm2

#	vmulpd %ymm13,%ymm3,%ymm8	
#	vsubpd %ymm8,%ymm2,%ymm2
	vfnmadd231pd %ymm3,%ymm13,%ymm2 # ymm2 = - ymm3 * ymm13 + ymm2
	
#	vmulpd %ymm12,%ymm3,%ymm8
#	vsubpd %ymm8,%ymm1,%ymm3
	vfnmadd213pd %ymm1,%ymm12,%ymm3 # ymm3 = - ymm3 * ymm12 + ymm1
	
#	vmulpd %ymm13,%ymm9,%ymm8	
#	vaddpd %ymm8,%ymm3,%ymm3
	vfmadd231pd %ymm9,%ymm13,%ymm3 # ymm3 = ymm9 * ymm13 + ymm3
	
#	vmulpd %ymm12,%ymm10,%ymm8		
#	vsubpd %ymm8,%ymm4,%ymm6
	vmovapd %ymm4,%ymm6
	vfnmadd231pd %ymm10,%ymm12,%ymm6 # ymm6 = - ymm10 * ymm12 + ymm6
	
#	vmulpd %ymm13,%ymm7,%ymm8	
#	vsubpd %ymm8,%ymm6,%ymm6
	vfnmadd231pd %ymm7,%ymm13,%ymm6 # ymm6 = - ymm7 * ymm13 + ymm6	
	
#	vmulpd %ymm12,%ymm7,%ymm8
#	vsubpd %ymm8,%ymm5,%ymm7
	vfnmadd213pd %ymm5,%ymm12,%ymm7 # ymm7 = - ymm7 * ymm12 + ymm5
	
#	vmulpd %ymm13,%ymm10,%ymm8			
#	vaddpd %ymm8,%ymm7,%ymm7
	vfmadd231pd %ymm10,%ymm13,%ymm7 # ymm7 = ymm10 * ymm13 + ymm7
	
#	vmulpd %ymm11,%ymm0,%ymm0
#	vsubpd %ymm2,%ymm0,%ymm0
	vfmsub213pd %ymm2,%ymm11,%ymm0 # ymm0 = ymm0 * ymm11 - ymm2
	
#	vmulpd %ymm11,%ymm1,%ymm1
#	vsubpd %ymm3,%ymm1,%ymm1
	vfmsub213pd %ymm3,%ymm11,%ymm1 # ymm1 = ymm1 * ymm11 - ymm3
	
#	vmulpd %ymm11,%ymm4,%ymm4
#	vsubpd %ymm6,%ymm4,%ymm4
	vfmsub213pd %ymm6,%ymm11,%ymm4 # ymm4 = ymm4 * ymm11 - ymm6
	
#	vmulpd %ymm11,%ymm5,%ymm5
#	vsubpd %ymm7,%ymm5,%ymm5
	vfmsub213pd %ymm7,%ymm11,%ymm5 # ymm5 = ymm5 * ymm11 - ymm7

#--
#	vmulpd %ymm14,%ymm4,%ymm8
#	vsubpd %ymm8,%ymm0,%ymm9
	vmovapd %ymm0,%ymm9
	vfnmadd231pd %ymm4,%ymm14,%ymm9 # ymm9 = - ymm4 * ymm14 + ymm9
#	vmulpd %ymm15,%ymm5,%ymm8	
#	vsubpd %ymm8,%ymm9,%ymm9
	vfnmadd231pd %ymm5,%ymm15,%ymm9 # ymm9 = - ymm5 * ymm15 + ymm9
	vmovapd %ymm9,(%r13,%r10)  # 01R	
	
#	vmulpd %ymm14,%ymm5,%ymm8
#	vsubpd %ymm8,%ymm1,%ymm5
	vfnmadd213pd %ymm1,%ymm14,%ymm5 # ymm5 = - ymm5 * ymm14 + ymm1
#	vmulpd %ymm15,%ymm4,%ymm8	
#	vaddpd %ymm8,%ymm5,%ymm5
	vfmadd231pd %ymm4,%ymm15,%ymm5 # ymm5 = ymm4 * ymm15 + ymm5
	vmovapd %ymm5,32(%r13,%r10)   # 01I
	
#	vmulpd %ymm11,%ymm0,%ymm0
#	vsubpd %ymm9,%ymm0,%ymm0
	vfmsub213pd %ymm9,%ymm11,%ymm0 # ymm0 = ymm0 * ymm11 - ymm9
	vmovapd %ymm0, (%r13,%r9)       # 00R
#	vmulpd %ymm11,%ymm1,%ymm1
#	vsubpd %ymm5,%ymm1,%ymm1
	vfmsub213pd %ymm5,%ymm11,%ymm1 # ymm1 = ymm1 * ymm11 - ymm5
	vmovapd %ymm1,32(%r13,%r9)       # 00I

#	vmulpd %ymm15,%ymm6,%ymm8
#	vaddpd %ymm8,%ymm2,%ymm9
	vmovapd %ymm2,%ymm9
	vfmadd231pd %ymm6,%ymm15,%ymm9 # ymm9 =  ymm6 * ymm15 + ymm9
#	vmulpd %ymm14,%ymm7,%ymm8	
#	vsubpd %ymm8,%ymm9,%ymm9
	vfnmadd231pd %ymm7,%ymm14,%ymm9 # ymm9 =  - ymm7 * ymm14 + ymm9	
	vmovapd %ymm9, (%r13,%r12)   # 11R	
	
#	vmulpd %ymm15,%ymm7,%ymm8
#	vaddpd %ymm8,%ymm3,%ymm7
	vfmadd213pd %ymm3,%ymm15,%ymm7 # ymm7 = ymm7 * ymm15 + ymm3
#	vmulpd %ymm14,%ymm6,%ymm8	
#	vaddpd %ymm8,%ymm7,%ymm7
	vfmadd231pd %ymm6,%ymm14,%ymm7 # ymm7 =  ymm6 * ymm14 + ymm7
	vmovapd %ymm7,32(%r13,%r12)   # 11I	

#	vmulpd %ymm11,%ymm2,%ymm2
#	vsubpd %ymm9,%ymm2,%ymm2
	vfmsub213pd %ymm9,%ymm11,%ymm2 # ymm2 = ymm2 * ymm11 - ymm9
	vmovapd %ymm2, (%r13,%r11)   # 10R	
#	vmulpd %ymm11,%ymm3,%ymm3
#	vsubpd %ymm7,%ymm3,%ymm3
	vfmsub213pd %ymm7,%ymm11,%ymm3 # ymm3 = ymm3 * ymm11 - ymm7
	vmovapd %ymm3,32(%r13,%r11)   # 10I
	
#-----
	addq $64,%r13
	cmpq %r13,%rbp
	jne L2

	addq $1,%rax		
	lea (%rdi,%r12),%r9
	cmpq %rax,%rdx
	jne L19

LE:	


	popq %r15	
	popq %r14
	popq %r13	
	popq %r12
	popq %rbp		
	popq %rbx
       
	ret
       
C2: # 倍精度不動小数点の 2
	.long   0x00000000,0x40000000
	
