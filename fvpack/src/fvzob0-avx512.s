########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl fvzob0_
.globl _fvzob0_	
fvzob0_:
_fvzob0_:	
	movl (%rdi), %edi  # NHH が rdi に
      # X の先頭アドレスは rsi

	vbroadcastsd C2(%rip),%zmm11 # 倍精度不動小数点の 2 を zmm11 の4箇所に

	shlq $7,%rdi # rdi に NHH*128 が

# I=0 の場合	

	lea (%rdi,%rdi),%rcx
	lea (%rdi,%rcx),%rdx
	lea (%rdi,%rsi),%rax

L1:
	vmovapd  (%rsi),   %zmm0 # 00R
	vmovapd 64(%rsi),  %zmm1 # 00I	
	vmovapd  (%rsi,%rcx),  %zmm2 # 10R
	vmovapd 64(%rsi,%rcx),  %zmm3 # 10I
	vmovapd  (%rsi,%rdi),  %zmm4 # 01R
	vmovapd 64(%rsi,%rdi),  %zmm5 # 01I
	vmovapd  (%rsi,%rdx),  %zmm6 # 11R
	vmovapd 64(%rsi,%rdx),  %zmm7 # 11I

	vsubpd %zmm2,%zmm0,%zmm2
	vsubpd %zmm3,%zmm1,%zmm3
	
	vfmsub213pd %zmm2,%zmm11,%zmm0 # zmm0 = zmm0 * zmm11 - zmm2
	vfmsub213pd %zmm3,%zmm11,%zmm1 # zmm1 = zmm1 * zmm11 - zmm3	

	vsubpd %zmm6,%zmm4,%zmm6
	vsubpd %zmm7,%zmm5,%zmm7

	vfmsub213pd %zmm6,%zmm11,%zmm4 # zmm4 = zmm4 * zmm11 - zmm6	
	
	vfmsub213pd %zmm7,%zmm11,%zmm5 # zmm5 = zmm5 * zmm11 - zmm7	

#--
	vsubpd %zmm4,%zmm0,%zmm9
	vmovapd %zmm9,(%rsi,%rdi)  # 01R		
	vsubpd %zmm5,%zmm1,%zmm5
	vmovapd %zmm5,64(%rsi,%rdi)   # 01I	
	
	vfmsub213pd %zmm9,%zmm11,%zmm0 # zmm0 = zmm0 * zmm11 - zmm9	
	vmovapd %zmm0, (%rsi)       # 00R	
	vfmsub213pd %zmm5,%zmm11,%zmm1 # zmm1 = zmm1 * zmm11 - zmm5	
	vmovapd %zmm1,64(%rsi)       # 00I	
	
	vaddpd %zmm7,%zmm2,%zmm9
	vmovapd %zmm9, (%rsi,%rdx)   # 11R		
	
	vsubpd %zmm6,%zmm3,%zmm7
	vmovapd %zmm7,64(%rsi,%rdx)   # 11I		

	vfmsub213pd %zmm9,%zmm11,%zmm2 # zmm2 = zmm2 * zmm11 - zmm9	
	vmovapd %zmm2, (%rsi,%rcx)   # 10R		
	vfmsub213pd %zmm7,%zmm11,%zmm3 # zmm3 = zmm3 * zmm11 - zmm7	
	vmovapd %zmm3,64(%rsi,%rcx)   # 10I	
	
	
#-----
	addq $128,%rsi
	cmpq %rsi,%rax
	jne L1

	ret
       
C2: # 倍精度不動小数点の 2
	.long   0x00000000,0x40000000
	
