########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl fvzob4_
.globl _fvzob4_	
fvzob4_:
_fvzob4_:	
	pushq %rbx
	pushq %rbp	
	pushq %r12
	pushq %r13
	pushq %r14
	pushq %r15	
	
	movl (%rdi), %edi  # KHH が rdi に
	movl (%rsi), %esi # LS が rsi に
	movl (%rdx), %edx # LL が rdx に		
      # X の先頭アドレスは rcx
      # T の先頭アドレスは r8

	vbroadcastsd C2(%rip),%zmm11 # 倍精度不動小数点の 2 を zmm11 の4箇所に

	movq %rsi,%rax # rax に LS
	addq %rsi,%rdx # rdx に LS+LL (終了条件)

	shlq $7,%rdi # KHH*M*8*2=KHH*128
	movq %rsi,%r9 # r9 に LS	
	imulq %rdi,%r9 # r9 に LS*KHH*128
	shlq $2,%r9 # r9 に LS*KHH*128*4


# %rax を I のためのカウンタとする

L1:

# %rbp を J*16*4 のためのカウンタとする


	cmpq $0,%rax
	jne L19

# I=0 の場合	

	lea (%rdi,%r9),%r10
	lea (%rdi,%r10),%r11
	lea (%rdi,%r11),%r12	
	

	xorq %rbp,%rbp
	movq %rcx,%r13
#.align 16
L18:
	vmovapd  (%r13,%r9),   %zmm0 # 00R
	vmovapd 64(%r13,%r9),  %zmm1 # 00I	
	vmovapd  (%r13,%r11),  %zmm2 # 10R
	vmovapd 64(%r13,%r11),  %zmm3 # 10I
	vmovapd  (%r13,%r10),  %zmm4 # 01R
	vmovapd 64(%r13,%r10),  %zmm5 # 01I
	vmovapd  (%r13,%r12),  %zmm6 # 11R
	vmovapd 64(%r13,%r12),  %zmm7 # 11I

	vsubpd %zmm2,%zmm0,%zmm2
	vsubpd %zmm3,%zmm1,%zmm3
	
	vfmsub213pd %zmm2,%zmm11,%zmm0 # zmm0 = zmm0 * zmm11 - zmm2
	vfmsub213pd %zmm3,%zmm11,%zmm1 # zmm1 = zmm1 * zmm11 - zmm3	

	vsubpd %zmm6,%zmm4,%zmm6
	vsubpd %zmm7,%zmm5,%zmm7

	vfmsub213pd %zmm6,%zmm11,%zmm4 # zmm4 = zmm4 * zmm11 - zmm6	
	vfmsub213pd %zmm7,%zmm11,%zmm5 # zmm5 = zmm5 * zmm11 - zmm7	

#--
	vsubpd %zmm4,%zmm0,%zmm9
	vmovapd %zmm9,(%r13,%r10)  # 01R		
	vsubpd %zmm5,%zmm1,%zmm5
	vmovapd %zmm5,64(%r13,%r10)   # 01I	
	
	vfmsub213pd %zmm9,%zmm11,%zmm0 # zmm0 = zmm0 * zmm11 - zmm9	
	vmovapd %zmm0, (%r13,%r9)       # 00R	
	vfmsub213pd %zmm5,%zmm11,%zmm1 # zmm1 = zmm1 * zmm11 - zmm5	
	vmovapd %zmm1,64(%r13,%r9)       # 00I	
	
	vaddpd %zmm7,%zmm2,%zmm9
	vmovapd %zmm9, (%r13,%r12)   # 11R		
	
	vsubpd %zmm6,%zmm3,%zmm7
	vmovapd %zmm7,64(%r13,%r12)   # 11I		

	vfmsub213pd %zmm9,%zmm11,%zmm2 # zmm2 = zmm2 * zmm11 - zmm9	
	vmovapd %zmm2, (%r13,%r11)   # 10R		
	vfmsub213pd %zmm7,%zmm11,%zmm3 # zmm3 = zmm3 * zmm11 - zmm7	
	vmovapd %zmm3,64(%r13,%r11)   # 10I	
	
#-----
	addq $128,%r13
	addq $128,%rbp # J のカウンタ増やす	
	cmpq %rbp,%rdi
	jne L18

	lea (%rdi,%r12),%r9
	addq $1,%rax	
	cmpq %rax,%rdx
	je LE
	
#----------------------- I=0以外の場合 -----------	

L19:

	lea (%rdi,%r9),%r10
	lea (%rdi,%r10),%r11
	lea (%rdi,%r11),%r12	
	

	movq %rax,%rbx
	shlq $4,%rbx # KHI0 に対応するアドレス
	vbroadcastsd  (%r8,%rbx),%zmm12 # T(1,KHI0)
	vbroadcastsd 8(%r8,%rbx),%zmm13 # T(2,KHI0)
	shlq $1,%rbx # KHHI0 に対応するアドレス	
	vbroadcastsd  (%r8,%rbx),%zmm14 # T(1,KHHI0)
	vbroadcastsd 8(%r8,%rbx),%zmm15 # T(2,KHHI0)
	
	movq %rcx,%r13
	movq %rcx,%rbp	
	addq %rdi,%rbp
#.align 16
L2:
	vmovapd  (%r13,%r9),   %zmm0 # 00R
	vmovapd 64(%r13,%r9),  %zmm1 # 00I	
	vmovapd  (%r13,%r11),  %zmm9 # 10R		
	vmovapd 64(%r13,%r11),  %zmm3 # 10I
	vmovapd  (%r13,%r10),  %zmm4 # 01R
	vmovapd 64(%r13,%r10),  %zmm5 # 01I
	vmovapd  (%r13,%r12),  %zmm10 # 11R
	vmovapd 64(%r13,%r12),  %zmm7 # 11I


	vmovapd %zmm0,%zmm2		
	vfnmadd231pd %zmm9,%zmm12,%zmm2 # zmm2 = - zmm9 * zmm12 + zmm2

	vfmadd231pd %zmm3,%zmm13,%zmm2 # zmm2 = zmm3 * zmm13 + zmm2
	vfnmadd213pd %zmm1,%zmm12,%zmm3 # zmm3 = - zmm3 * zmm12 + zmm1
	
	vfnmadd231pd %zmm9,%zmm13,%zmm3 # zmm3 = - zmm9 * zmm13 + zmm3
	vmovapd %zmm4,%zmm6
	vfnmadd231pd %zmm10,%zmm12,%zmm6 # zmm6 = - zmm10 * zmm12 + zmm6
	
	vfmadd231pd %zmm7,%zmm13,%zmm6 # zmm6 = zmm7 * zmm13 + zmm6	
	
	vfnmadd213pd %zmm5,%zmm12,%zmm7 # zmm7 = - zmm7 * zmm12 + zmm5
	
	vfnmadd231pd %zmm10,%zmm13,%zmm7 # zmm7 = - zmm10 * zmm13 + zmm7
	
	vfmsub213pd %zmm2,%zmm11,%zmm0 # zmm0 = zmm0 * zmm11 - zmm2
	
	vfmsub213pd %zmm3,%zmm11,%zmm1 # zmm1 = zmm1 * zmm11 - zmm3
	
	vfmsub213pd %zmm6,%zmm11,%zmm4 # zmm4 = zmm4 * zmm11 - zmm6
	
	vfmsub213pd %zmm7,%zmm11,%zmm5 # zmm5 = zmm5 * zmm11 - zmm7

#--
	vmovapd %zmm0,%zmm9
	vfnmadd231pd %zmm4,%zmm14,%zmm9 # zmm9 = - zmm4 * zmm14 + zmm9
	vfmadd231pd %zmm5,%zmm15,%zmm9 # zmm9 = zmm5 * zmm15 + zmm9
	vmovapd %zmm9,(%r13,%r10)  # 01R	
	
	vfnmadd213pd %zmm1,%zmm14,%zmm5 # zmm5 = - zmm5 * zmm14 + zmm1
	vfnmadd231pd %zmm4,%zmm15,%zmm5 # zmm5 = - zmm4 * zmm15 + zmm5
	vmovapd %zmm5,64(%r13,%r10)   # 01I
	
	vfmsub213pd %zmm9,%zmm11,%zmm0 # zmm0 = zmm0 * zmm11 - zmm9
	vmovapd %zmm0, (%r13,%r9)       # 00R
	vfmsub213pd %zmm5,%zmm11,%zmm1 # zmm1 = zmm1 * zmm11 - zmm5
	vmovapd %zmm1,64(%r13,%r9)       # 00I

	vmovapd %zmm2,%zmm9
	vfmadd231pd %zmm6,%zmm15,%zmm9 # zmm9 =  zmm6 * zmm15 + zmm9
	vfmadd231pd %zmm7,%zmm14,%zmm9 # zmm9 =  zmm7 * zmm14 + zmm9	
	vmovapd %zmm9, (%r13,%r12)   # 11R	
	
	vfmadd213pd %zmm3,%zmm15,%zmm7 # zmm7 = zmm7 * zmm15 + zmm3
	vfnmadd231pd %zmm6,%zmm14,%zmm7 # zmm7 =  - zmm6 * zmm14 + zmm7
	vmovapd %zmm7,64(%r13,%r12)   # 11I	

	vfmsub213pd %zmm9,%zmm11,%zmm2 # zmm2 = zmm2 * zmm11 - zmm9
	vmovapd %zmm2, (%r13,%r11)   # 10R	
	vfmsub213pd %zmm7,%zmm11,%zmm3 # zmm3 = zmm3 * zmm11 - zmm7
	vmovapd %zmm3,64(%r13,%r11)   # 10I
	
#-----
	addq $128,%r13
	cmpq %r13,%rbp
	jne L2

	addq $1,%rax		
	lea (%rdi,%r12),%r9
	cmpq %rax,%rdx
	jne L19

LE:	


	popq %r15	
	popq %r14
	popq %r13	
	popq %r12
	popq %rbp		
	popq %rbx
       
	ret
       
C2: # 倍精度不動小数点の 2
	.long   0x00000000,0x40000000
	
