************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
      SUBROUTINE FVZOF2(NH,X)

      IMPLICIT REAL*8(A-H,O-Z)      
      PARAMETER(M=8)
      DIMENSION X(M,2,0:*)

      F=1D0/(2*NH)

      DO J=0,NH-1
        DO IV=1,M
          X(IV,1,J)=F*X(IV,1,J)+F*X(IV,1,J+NH)
          X(IV,2,J)=F*X(IV,2,J)+F*X(IV,2,J+NH)
          X(IV,1,J+NH)=X(IV,1,J)-2*F*X(IV,1,J+NH)
          X(IV,2,J+NH)=X(IV,2,J)-2*F*X(IV,2,J+NH)
        END DO
      END DO

      END
