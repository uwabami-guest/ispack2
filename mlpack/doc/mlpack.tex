%***********************************************************************
% ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
% Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
%
% This library is free software; you can redistribute it and/or
% modify it under the terms of the GNU Lesser General Public
% License as published by the Free Software Foundation; either
% version 2.1 of the License, or (at your option) any later version.
%
% This library is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public
% License along with this library; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
% 02110-1301 USA.
%***********************************************************************
%
% last modified 2016/12/23
%
\documentclass[a4paper]{scrartcl}

\title{Manual of MLPACK}
\author{}
\date{}

\begin{document}

\maketitle

\section{Outline}

This is a package of miscllaneous subroutines.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{List of subroutines}

\vspace{1em}
\begin{tabular}{ll}
\texttt{MLTIME} & get the current UNIX-time\\
\texttt{MLALLC} & allocate a memory space aligned with 64byte boundary\\
\texttt{MLFREE} & free a memory space\\
\texttt{MLRAND} & generate a uniform pseudo-random number\\
\texttt{MLNORM} & generate a normal pseudo-random number
\end{tabular}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Usage of each subroutine}

\subsection{MLTIME}

\begin{enumerate}

\item Purpose

Show the UNIX-time, the number of seconds that have elapsed since
00:00:00 UTC, 1 January 1970.

\item Definition

\item Synopsis
    
\texttt{MLTIME(SEC)}
  
\item Parameters
    
\begin{tabular}{lll}
\texttt{SEC} & \texttt{(D)} & Output. the current UNIX-time (in seconds).
\end{tabular}

\item Remarks

\end{enumerate}

%----------------------------------------------------------------------

\subsection{MLALLC}

\begin{enumerate}

\item Purpose

Allocate a 64-byte aligned memory.

\item Definition

If you have made ISPACK with setting SSE=avx or SSE=fma
on Intel x86 CPUs, some arrays for FVPACK, LVPACK, and
SVPACK must be 32-byte aligned, or must be 64-byte aligned
with setting SSE=avx512.
However, not all compilers
do the alignment automatically (Intel ifort can do it 
with the option ``-align array64byte'', but gfortran does
not have such an option).
This subroutine enables such a memory allocation on a 
general Fortran90 environment.

\item Synopsis
    
\texttt{MLALLC(P,N)}
  
\item Parameters
    
\begin{tabular}{lll}
\texttt{P} & \texttt{(TYPE(C\_PTR))} & Output. 
A \texttt{C\_PTR}-type structure which contains the pointer\\
& & 
to the beginning address of the allocated memory.\\
\texttt{N} & \texttt{(I)} & Input. The size of the double-precision array
to be allocated\\
   &   & (namely, \texttt{N*8}-bytes of memory is allocated).
\end{tabular}

\item Remarks

In an Fortran90 program, you can use 64-byte aligned 
double-precision arrays by using \texttt{MLALLC} with 
the \texttt{ISO\_C\_BINDING} module as the following example.
You must free the allocated memory by calling \texttt{MLFREE}
before ending the program.

\begin{verbatim}
      USE ISO_C_BINDING
      IMPLICIT NONE
      INTEGER,PARAMETER :: M=20,N=10
      INTEGER :: I,J
      REAL(8),DIMENSION(:,:),POINTER:: A
      TYPE(C_PTR) :: P

      CALL MLALLC(P,M*N) ! memory allocation 
      CALL C_F_POINTER(P,A,[M,N]) ! associate a pointer with an array
      ! hereinafter, you can use a double-precision array A(M,N).

      DO J=1,N
         DO I=1,M
            A(I,J)=1000*I+J
         END DO
      END DO

      DO J=1,N
         DO I=1,M
            PRINT *,A(I,J)
         END DO
      END DO

      CALL MLFREE(P) ! free the memory that is allocated by MLALLC
        
      END
\end{verbatim}

\end{enumerate}

%----------------------------------------------------------------------
\subsection{MLFREE}

\begin{enumerate}

\item Purpose 

Free a memory space.

\item Definition

Free the memory allocated by \texttt{MLALLC}.

\item Synopsis 
    
\texttt{MLFREE(P)}
  
\item Parameters 
    
\begin{tabular}{lll}
\texttt{P} & \texttt{(TYPE(C\_PTR))} & Input. 
The \texttt{C\_PTR}-type structure that contains\\
& & the pointer when a memmory is allocated by \texttt{MLALLC}.
\end{tabular}

\item Remark

See also \texttt{MLALLC} for the usage example.

\end{enumerate}

%----------------------------------------------------------------------


\subsection{MLRAND}

\begin{enumerate}

\item Purpose 

Generate a uniform pseudo-random number by the linear congruential method.

\item Definition

Generate a uniform pseudo-random number by the linear congruential method
and convert it into a uniform random number in $(0,1)$.
The quality of the random number is not good because it is based 
on the simple linear congruential method.
If the quality of the random number must be high for 
the Monte Carlo method, for example, you must use more sophisticated
pseudo-random number generators like the Mersenne twister, instead.

\item Synopsis 
    
\texttt{MLRAND(ISEED,R)}
  
\item Parameters 
    
\begin{tabular}{lll}
\texttt{ISEED} & \texttt{(I)} & Input and output. The seed of the
random number\\
\texttt{R} & \texttt{(D)} & Output. A uniform random number in $(0,1)$
\end{tabular}

\item Remark

In using, \texttt{MLRAND(ISEED,R)} should be called repeatedly
after setting an appropriate value to \texttt{ISEED} initially.

\end{enumerate}

%----------------------------------------------------------------------

\subsection{MLNORM}

\begin{enumerate}

\item Purpose 

Generate a normal pseudo-random number by 
the Box-Muller method.

\item Definition

Generate a normal pseudo-random number by 
the Box-Muller method. This program uses
\texttt{MLRAND} as the uniform pseudo-random generator.
Therefore, you should use another program which is based 
on a better random number generator like the Mersenne twister instead
when the quality of the random number is crucial.

\item Synopsis 
    
\texttt{MLNORM(ISEED,R)}
  
\item Parameters 
    
\begin{tabular}{lll}
\texttt{ISEED} & \texttt{(I)} & Input and output. The seed of the
random number\\
\texttt{R} & \texttt{(D)} & Output. A normal pseudo-random number \\
& & of zero mean and unit standard deviation.
\end{tabular}

\item Remark

In using, \texttt{MLNORM(ISEED,R)} should be called repeatedly
after setting an appropriate value to \texttt{ISEED} initially.

\end{enumerate}

\end{document}
