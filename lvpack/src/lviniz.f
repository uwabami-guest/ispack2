************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
      SUBROUTINE LVINIZ(MM,NM,JM,P,R)

      IMPLICIT REAL*8(A-H,O-P,R-Z)
      IMPLICIT REAL*16(Q)
      PARAMETER(NB=128)
      DIMENSION P(JM/2,5)
      DIMENSION R(NM/2*3+NM)
      DIMENSION QER(NB)
      QE(N,M)=SQRT(1Q0*(N-M)*(N+M)/((2Q0*N-1)*(2*N+1)))

      JH=JM/2

      QEPS=1
      DO I=1,NB
        QEPS=QEPS/2
        QER(I)=QEPS+1
      END DO

      I=0
      QEPS=1
   10 CONTINUE
        I=I+1
        QEPS=QEPS/2
      IF(QER(I).GT.1) GOTO 10

      QEPS=QEPS*16

      QPI=4*ATAN(1Q0)

!$omp parallel private(QZ,N,IFLAG,QP0,QP1,QDP,QDZ)
!$omp do schedule(dynamic)
      DO J=1,JH
        QZ=SIN(QPI*(2*J-1)/(2*JM+1))
        IFLAG=0
   20   CONTINUE
          QP0=0
          QP1=1
          DO N=1,JM-1,2
            QP0=((2*N-1)*QZ*QP1-(N-1)*QP0)/N
            QP1=((2*N+1)*QZ*QP0-N*QP1)/(N+1)
          END DO
          QDP=JM*(QP0-QZ*QP1)/(1-QZ*QZ)
          QDZ=QP1/QDP
          QZ=QZ-QDZ
        IF(IFLAG.EQ.0) THEN
          IF(ABS(QDZ/QZ).LE.QEPS) THEN
            IFLAG=1
          END IF
          GOTO 20
        END IF
        P(J,1)=QZ
        P(J,2)=1/(QDP*QDP)/(1-QZ*QZ)
        P(J,3)=SQRT(1-QZ*QZ)
        P(J,4)=1/SQRT(1-QZ*QZ)
        P(J,5)=QZ*QZ
      END DO
!$omp end do
!$omp end parallel

      M=0
      IF(NM.GE.M+1) THEN
        IE=(M*(2*NM-M)+1)/4*3+M*(2*NM-M+1)/2
        IA=IE+(NM-M)/2*2
        IC=IA+(NM-M+1)/2
        QR=SQRT(2Q0*M+3)
        DO L=1,(NM-M)/2
          N=M+2*L-1
          R(IA+L)=QR
          QR2=(-1)**(L-1)*QR*QR
          R(IC+2*L-1)=QR2
          R(IC+2*L)=-QR2*(QE(N+1,M)**2+QE(N,M)**2)
          QR=QR*QE(N+1,M)
          R(IE+2*L-1)=QR
          QR=(-1)**(L-1)/QR
          R(IE+2*L)=QR
          QR=QR/QE(N+2,M)
        END DO
        IF(MOD(NM-M-1,2).EQ.0) THEN
          L=(NM-M+1)/2
          R(IA+L)=QR
        END IF
      END IF

      END
