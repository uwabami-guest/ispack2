########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl lvogws_
.globl _lvogws_	
lvogws_:
_lvogws_:	
	movl (%rdi),%edi
	shlq $6,%rdi
	movq %rdi,%r8
	shlq $3,%rdi
	subq %r8,%rdi
	addq %rcx,%rdi

	vsubpd %zmm0,%zmm0,%zmm0
        vsubpd %zmm1,%zmm1,%zmm1
        vsubpd %zmm2,%zmm2,%zmm2
        vsubpd %zmm3,%zmm3,%zmm3
        vsubpd %zmm4,%zmm4,%zmm4
        vsubpd %zmm5,%zmm5,%zmm5
        vsubpd %zmm6,%zmm6,%zmm6
        vsubpd %zmm7,%zmm7,%zmm7
	
	vbroadcastsd   (%rsi),%zmm8
	vbroadcastsd  8(%rsi),%zmm9
	vbroadcastsd 16(%rsi),%zmm10
	vbroadcastsd 24(%rsi),%zmm11
L00:
	vmovapd   (%rcx),%zmm15
	vmovapd   (%rcx),%zmm14
	vmovapd 64(%rcx),%zmm12
	vmovapd 128(%rcx),%zmm13
	
	vfmadd213pd %zmm9,%zmm8,%zmm15
	vfmadd213pd %zmm11,%zmm10,%zmm14	
	
	vfmadd231pd 192(%rcx),%zmm12,%zmm1
	vfmadd231pd 192(%rcx),%zmm13,%zmm3	
	vfmadd231pd 256(%rcx),%zmm12,%zmm0	
	vfmadd231pd 256(%rcx),%zmm13,%zmm2
	vfmadd213pd %zmm12,%zmm13,%zmm15
	vfmadd231pd 320(%rcx),%zmm12,%zmm5
	vfmadd231pd 320(%rcx),%zmm13,%zmm7
	vfmadd231pd 384(%rcx),%zmm12,%zmm4
	vfmadd231pd 384(%rcx),%zmm13,%zmm6

	vmovapd %zmm15,64(%rcx)			
	vfmadd213pd %zmm13,%zmm15,%zmm14	
	vmovapd %zmm14,128(%rcx)
	
	addq $448,%rcx
	cmpq %rcx,%rdi
	jne L00

	vshuff64x2 $11,%zmm0,%zmm0,%zmm8
	vshuff64x2 $11,%zmm1,%zmm1,%zmm9
	vshuff64x2 $11,%zmm2,%zmm2,%zmm10
	vshuff64x2 $11,%zmm3,%zmm3,%zmm11	
	vshuff64x2 $11,%zmm4,%zmm4,%zmm12
	vshuff64x2 $11,%zmm5,%zmm5,%zmm13
	vshuff64x2 $11,%zmm6,%zmm6,%zmm14
	vshuff64x2 $11,%zmm7,%zmm7,%zmm15	
	
	vaddpd %ymm8,%ymm0,%ymm0
	vaddpd %ymm9,%ymm1,%ymm1
	vaddpd %ymm10,%ymm2,%ymm2
	vaddpd %ymm11,%ymm3,%ymm3
	vaddpd %ymm12,%ymm4,%ymm4
	vaddpd %ymm13,%ymm5,%ymm5
	vaddpd %ymm14,%ymm6,%ymm6
	vaddpd %ymm15,%ymm7,%ymm7
	
	vhaddpd %ymm0,%ymm0,%ymm0
	vhaddpd %ymm1,%ymm1,%ymm1
	vhaddpd %ymm2,%ymm2,%ymm2
	vhaddpd %ymm3,%ymm3,%ymm3
	vhaddpd %ymm4,%ymm4,%ymm4
	vhaddpd %ymm5,%ymm5,%ymm5
	vhaddpd %ymm6,%ymm6,%ymm6
	vhaddpd %ymm7,%ymm7,%ymm7
	
	vextractf128 $1,%ymm0,%xmm8
	vextractf128 $1,%ymm1,%xmm9
	vextractf128 $1,%ymm2,%xmm10
	vextractf128 $1,%ymm3,%xmm11
	vextractf128 $1,%ymm4,%xmm12
	vextractf128 $1,%ymm5,%xmm13
	vextractf128 $1,%ymm6,%xmm14
	vextractf128 $1,%ymm7,%xmm15
	
	vaddsd (%rdx),%xmm8,%xmm8
	vaddsd 16(%rdx),%xmm9,%xmm9
	vaddsd 32(%rdx),%xmm10,%xmm10
	vaddsd 48(%rdx),%xmm11,%xmm11
	vaddsd 8(%rdx),%xmm12,%xmm12
	vaddsd 24(%rdx),%xmm13,%xmm13	
	vaddsd 40(%rdx),%xmm14,%xmm14
	vaddsd 56(%rdx),%xmm15,%xmm15
	
	vaddsd %xmm8,%xmm0,%xmm0
	vaddsd %xmm9,%xmm1,%xmm1
	vaddsd %xmm10,%xmm2,%xmm2
	vaddsd %xmm11,%xmm3,%xmm3
	vaddsd %xmm12,%xmm4,%xmm4
	vaddsd %xmm13,%xmm5,%xmm5
	vaddsd %xmm14,%xmm6,%xmm6
	vaddsd %xmm15,%xmm7,%xmm7
	
	vmovsd %xmm0,(%rdx)
	vmovsd %xmm1,16(%rdx)
	vmovsd %xmm2,32(%rdx)
	vmovsd %xmm3,48(%rdx)
	vmovsd %xmm4,8(%rdx)
	vmovsd %xmm5,24(%rdx)
	vmovsd %xmm6,40(%rdx)
	vmovsd %xmm7,56(%rdx)
	
	ret
