************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
      SUBROUTINE LVSGWS(NM,NN,JM,JV,JR,M,S,G,P,PM,Q,R,JC,WS,IPOW,IFLAG)

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION P(JM/2,5),PM(JM/2,2)
      DIMENSION R(*)
      DIMENSION S(2,M:NN)
      DIMENSION WS(2,M:NN) ! 使わないけど..
      DIMENSION Q(JV,0:6,JR),G(JM*2),JC(*)

      IE=0
      IA=IE+(NM-M)/2*2
      IC=IA+(NM-M+1)/2

      IJ=0

      CALL LVSET0((NN+1-M)*2,S)

      DO ID=1,JM/(2*JV*JR)

        L=0
        DO IR=1,JR
          JD=IR+JR*(ID-1)
          DO J=1,JV
            Q(J,0,IR)=P(J+JV*(JD-1),5)
          END DO
          JCT=MIN(JV,JC(IJ+1)-JV*(JD-1))
          DO J=1,JCT
            Q(J,1,IR)=PM(J+JV*(JD-1),1)
            Q(J,2,IR)=PM(J+JV*(JD-1),2)
          END DO
          DO J=MAX(1,JCT+1),JV
            Q(J,1,IR)=0
            Q(J,2,IR)=0
          END DO
        END DO

        IF(IPOW.EQ.0) THEN
          IF(IFLAG.EQ.0) THEN   ! SVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                Q(J,4,IR)=(G(J+JM+JVD*2)+G(JV+1-J+JM-JD*JV*2))
     &              *P(J+JVD,2)
                Q(J,3,IR)=(G(J+JM+JVD*2)-G(JV+1-J+JM-JD*JV*2))
     &              *P(J+JVD,1)*P(J+JVD,2)
                Q(J,6,IR)=(G(J+JV+JM+JVD*2)+G(JV+1-J+JV+JM-JD*JV*2))
     &              *P(J+JVD,2)
                Q(J,5,IR)=(G(J+JV+JM+JVD*2)-G(JV+1-J+JV+JM-JD*JV*2))
     &              *P(J+JVD,1)*P(J+JVD,2)
              END DO
            END DO
          ELSE ! LVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                Q(J,4,IR)
     &              =(G(1+2*(JM/2+J+JVD-1))+G(1+2*(JM/2+1-(J+JVD)-1)))
     &              *P(J+JVD,2)
                Q(J,3,IR)
     &              =(G(1+2*(JM/2+J+JVD-1))-G(1+2*(JM/2+1-(J+JVD)-1)))
     &              *P(J+JVD,1)*P(J+JVD,2)
                Q(J,6,IR)
     &              =(G(2+2*(JM/2+J+JVD-1))+G(2+2*(JM/2+1-(J+JVD)-1)))
     &              *P(J+JVD,2)
                Q(J,5,IR)
     &              =(G(2+2*(JM/2+J+JVD-1))-G(2+2*(JM/2+1-(J+JVD)-1)))
     &              *P(J+JVD,1)*P(J+JVD,2)
              END DO
            END DO
          END IF
        ELSE IF(IPOW.EQ.1) THEN
          IF(IFLAG.EQ.0) THEN   ! SVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                Q(J,4,IR)=(G(J+JM+JVD*2)+G(JV+1-J+JM-JD*JV*2))
     &              *P(J+JVD,2)*P(J+JVD,4)
                Q(J,3,IR)=(G(J+JM+JVD*2)-G(JV+1-J+JM-JD*JV*2))
     &              *P(J+JVD,1)*P(J+JVD,2)*P(J+JVD,4)
                Q(J,6,IR)=(G(J+JV+JM+JVD*2)+G(JV+1-J+JV+JM-JD*JV*2))
     &              *P(J+JVD,2)*P(J+JVD,4)
                Q(J,5,IR)=(G(J+JV+JM+JVD*2)-G(JV+1-J+JV+JM-JD*JV*2))
     &              *P(J+JVD,1)*P(J+JVD,2)*P(J+JVD,4)
              END DO
            END DO
          ELSE ! LVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                Q(J,4,IR)
     &              =(G(1+2*(JM/2+J+JVD-1))+G(1+2*(JM/2+1-(J+JVD)-1)))
     &              *P(J+JVD,2)*P(J+JVD,4)
                Q(J,3,IR)
     &              =(G(1+2*(JM/2+J+JVD-1))-G(1+2*(JM/2+1-(J+JVD)-1)))
     &              *P(J+JVD,1)*P(J+JVD,2)*P(J+JVD,4)
                Q(J,6,IR)
     &              =(G(2+2*(JM/2+J+JVD-1))+G(2+2*(JM/2+1-(J+JVD)-1)))
     &              *P(J+JVD,2)*P(J+JVD,4)
                Q(J,5,IR)
     &              =(G(2+2*(JM/2+J+JVD-1))-G(2+2*(JM/2+1-(J+JVD)-1)))
     &              *P(J+JVD,1)*P(J+JVD,2)*P(J+JVD,4)
              END DO
            END DO
          END IF
        ELSE IF(IPOW.EQ.2) THEN
          IF(IFLAG.EQ.0) THEN   ! SVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                Q(J,4,IR)=(G(J+JM+JVD*2)+G(JV+1-J+JM-JD*JV*2))
     &              *P(J+JVD,2)*P(J+JVD,4)*P(J+JVD,4)
                Q(J,3,IR)=(G(J+JM+JVD*2)-G(JV+1-J+JM-JD*JV*2))
     &              *P(J+JVD,1)*P(J+JVD,2)*P(J+JVD,4)*P(J+JVD,4)
                Q(J,6,IR)=(G(J+JV+JM+JVD*2)+G(JV+1-J+JV+JM-JD*JV*2))
     &              *P(J+JVD,2)*P(J+JVD,4)*P(J+JVD,4)
                Q(J,5,IR)=(G(J+JV+JM+JVD*2)-G(JV+1-J+JV+JM-JD*JV*2))
     &              *P(J+JVD,1)*P(J+JVD,2)*P(J+JVD,4)*P(J+JVD,4)
              END DO
            END DO
          ELSE ! LVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                Q(J,4,IR)
     &              =(G(1+2*(JM/2+J+JVD-1))+G(1+2*(JM/2+1-(J+JVD)-1)))
     &              *P(J+JVD,2)*P(J+JVD,4)*P(J+JVD,4)
                Q(J,3,IR)
     &              =(G(1+2*(JM/2+J+JVD-1))-G(1+2*(JM/2+1-(J+JVD)-1)))
     &              *P(J+JVD,1)*P(J+JVD,2)*P(J+JVD,4)*P(J+JVD,4)
                Q(J,6,IR)
     &              =(G(2+2*(JM/2+J+JVD-1))+G(2+2*(JM/2+1-(J+JVD)-1)))
     &              *P(J+JVD,2)*P(J+JVD,4)*P(J+JVD,4)
                Q(J,5,IR)
     &              =(G(2+2*(JM/2+J+JVD-1))-G(2+2*(JM/2+1-(J+JVD)-1)))
     &              *P(J+JVD,1)*P(J+JVD,2)*P(J+JVD,4)*P(J+JVD,4)
              END DO
            END DO
          END IF
        END IF

        LD=1
        JC1=JC(IJ+LD)
        JB=MIN(JR,(JC1-1-JV*JR*(ID-1)+JV)/JV)

        DO N=M,NN-6,4
          L=(N-M)/2
          LD=L/2+1
          JC1=JC(IJ+LD)
          JB=MIN(JR,(JC1-1-JV*JR*(ID-1)+JV)/JV)
          IF(JB.GE.1) THEN
            IF(JV.EQ.4) THEN
              CALL LVQGWS(JB,R(IC+2*L+3),S(1,N),Q)
            ELSE IF(JV.EQ.8) THEN
              CALL LVOGWS(JB,R(IC+2*L+3),S(1,N),Q)
            ELSE
              CALL LVLGWS(JV,JB,R(IC+2*L+3),S(1,N),Q)
            END IF
          END IF
          JC1=JC(IJ+LD)
          JC2=JC(IJ+LD+1)
          IF(JC2.GT.JC1) THEN
            ID1=(JC1-1)/(JV*JR)+1
            ID2=(JC2-1)/(JV*JR)+1
            JB=MIN(JR,(JC2-1-JV*JR*(ID-1)+JV)/JV)
            IF(ID.GE.ID1.AND.ID.LE.ID2) THEN
              JS=MAX(1,(JC1-1-(ID-1)*JV*JR)/JV+1)
              JE=MIN(JR,(JC2-1-(ID-1)*JV*JR)/JV+1)
              DO IR=JS,JE
                JD=IR+JR*(ID-1)
                JCT1=MAX(0,JC1-JV*(JD-1))
                JCT2=MIN(JV,JC2-JV*(JD-1))
                DO J=JCT1+1,JCT2
                  Q(J,1,IR)=PM(J+JV*(JD-1),1)
                  Q(J,2,IR)=PM(J+JV*(JD-1),2)
                END DO
              END DO
            END IF
          END IF
        END DO

        IF(JB.GE.1) THEN
          L=(N-M)/2
          IF(NN-N.EQ.0) THEN
            CALL LV0GWS(JV,JB,S(1,N),Q)
          ELSE IF(NN-N.EQ.1) THEN
            CALL LV1GWS(JV,JB,S(1,N),Q)
          ELSE IF(NN-N.EQ.2) THEN
            CALL LV2GWS(JV,JB,S(1,N),Q)
          ELSE IF(NN-N.EQ.3) THEN
            CALL LV3GWS(JV,JB,S(1,N),Q)
          ELSE IF(NN-N.EQ.4) THEN
            CALL LV4GWS(JV,JB,R(IC+2*L+3),S(1,N),Q)
          ELSE IF(NN-N.EQ.5) THEN
            CALL LV5GWS(JV,JB,R(IC+2*L+3),S(1,N),Q)
          END IF
        END IF

      END DO

      DO L=1,(NN+1-M)/2
        N=2*L-1+M
        S(1,N)=R(IA+L)*S(1,N)
        S(2,N)=R(IA+L)*S(2,N)
      END DO

      DO N=(NN-M)/2*2+M,M+2,-2
        S(1,N)=R(IE+N-M)*S(1,N)+R(IE+N-M-1)*S(1,N-2) 
        S(2,N)=R(IE+N-M)*S(2,N)+R(IE+N-M-1)*S(2,N-2) 
      END DO

      END
