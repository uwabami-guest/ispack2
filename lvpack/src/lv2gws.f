************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
      SUBROUTINE LV2GWS(JV,JB,SD,Q)

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION SD(2,3),Q(JV,0:6,*)

      DO IR=1,JB
        DO J=1,JV
          SD(1,2)=SD(1,2)+Q(J,3,IR)*Q(J,1,IR)
          SD(1,1)=SD(1,1)+Q(J,4,IR)*Q(J,1,IR)
          SD(1,3)=SD(1,3)+Q(J,4,IR)*Q(J,2,IR)
          SD(2,2)=SD(2,2)+Q(J,5,IR)*Q(J,1,IR)
          SD(2,1)=SD(2,1)+Q(J,6,IR)*Q(J,1,IR)
          SD(2,3)=SD(2,3)+Q(J,6,IR)*Q(J,2,IR)
        END DO
      END DO

      END
