************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
      SUBROUTINE LVINIW(MM,NM,JM,M,P,PM,R,JC,WP)

      IMPLICIT REAL*8(A-H,O-P,R-Z)
      IMPLICIT REAL*16(Q)
      DIMENSION P(JM/2,5),PM(JM/2,2),WP(JM/2)
      DIMENSION R((NM-M)/2*3+NM-M)
      DIMENSION JC((NM-M)/4+1)
      QE(N,M)=SQRT(1Q0*(N-M)*(N+M)/((2Q0*N-1)*(2*N+1)))

      JH=JM/2

      IF(NM.GE.M+1) THEN
        IE=0
        IA=IE+(NM-M)/2*2
        IC=IA+(NM-M+1)/2
        QR=SQRT(2Q0*M+3)
        DO L=1,(NM-M)/2
          N=M+2*L-1
          R(IA+L)=QR
          QR2=(-1)**(L-1)*QR*QR
          R(IC+2*L-1)=QR2
          R(IC+2*L)=-QR2*(QE(N+1,M)**2+QE(N,M)**2)
          QR=QR*QE(N+1,M)
          R(IE+2*L-1)=QR
          QR=(-1)**(L-1)/QR
          R(IE+2*L)=QR
          QR=QR/QE(N+2,M)
        END DO
        IF(MOD(NM-M-1,2).EQ.0) THEN
          L=(NM-M+1)/2
          R(IA+L)=QR
        END IF
      END IF

*------------------------

      EPS=1D-20
      QPM=0
      DO MD=1,M
        QPM=QPM+0.5Q0*LOG(1Q0*(2*MD+1)/(2*MD))
      END DO

*      DO IJ=1,MM*(2*NM-MM-1)/8+MM
      DO IJ=1,(NM-M)/4+1
        JC(IJ)=0
      END DO

      DBIG=1D270
      DBIGR=1/DBIG
      DBIGL=LOG(DBIG)

*      IE=(M*(2*NM-M)+1)/4*3+M*(2*NM-M+1)/2
      IE=0
      IA=IE+(NM-M)/2*2
      IC=IA+(NM-M+1)/2

*      IJ=(M-1)*(2*NM-M)/8+M-1
      IJ=0

      IF(NM.LE.M+1) THEN
        L=0
        LD=1
        DO J=1,JH
          PM(J,1)=1
          PM(J,2)=0
        END DO
        DO J=JH,1,-1
          DCHECK=QPM+LOG(ABS(PM(J,1)))+M*LOG(P(J,3))
          IF(DCHECK.GT.LOG(EPS)) THEN
            JC(IJ+LD)=J
            GOTO 50
          END IF
        END DO
   50   CONTINUE
        DO J=1,JC(IJ+LD)
          DCHECK=QPM+LOG(ABS(PM(J,1)))+M*LOG(P(J,3))
          PM(J,1)=SIGN(EXP(DCHECK),PM(J,1))
        END DO
      ELSE
        L=0
        LD=1
        DO J=1,JH
          PM(J,1)=1
          PM(J,2)=(R(IC+2*L+1)*P(J,5)+R(IC+2*L+2))
        END DO
        DO J=JH,1,-1
          DCHECK=QPM+LOG(ABS(PM(J,1)))+M*LOG(P(J,3))
          DCHEC2=QPM+LOG(ABS(PM(J,2)))+M*LOG(P(J,3))
          IF(DCHEC2.GT.LOG(EPS).OR.DCHECK.GT.LOG(EPS)) THEN
            JC(IJ+LD)=J
            GOTO 100
          END IF
        END DO
  100   CONTINUE
        DO J=1,JC(IJ+LD)
          DCHECK=QPM+LOG(ABS(PM(J,1)))+M*LOG(P(J,3))
          DCHEC2=QPM+LOG(ABS(PM(J,2)))+M*LOG(P(J,3))
          PM(J,1)=SIGN(EXP(DCHECK),PM(J,1))
          PM(J,2)=SIGN(EXP(DCHEC2),PM(J,2))
        END DO

        DO J=JC(IJ+LD)+1,JH
          WP(J)=QPM+M*LOG(P(J,3))
        END DO

        DO N=M,NM-6,4
          L=(N-M)/2
          LD=L/2+1

          JC(IJ+LD+1)=JC(IJ+LD)
          DO J=JC(IJ+LD)+1,JH
            PM(J,1)=PM(J,1)
     &          +(R(IC+2*L+3)*P(J,5)+R(IC+2*L+4))*PM(J,2)
            PM(J,2)=PM(J,2)
     &          +(R(IC+2*L+5)*P(J,5)+R(IC+2*L+6))*PM(J,1)
            IF(ABS(PM(J,1)).GT.DBIG
     &          .OR.ABS(PM(J,2)).GT.DBIG) THEN
              PM(J,2)=PM(J,2)*DBIGR
              PM(J,1)=PM(J,1)*DBIGR
              WP(J)=WP(J)+DBIGL
            END IF
          END DO
          DO J=JH,JC(IJ+LD)+1,-1
            DCHECK=LOG(ABS(PM(J,1)))+WP(J)
            DCHEC2=LOG(ABS(PM(J,2)))+WP(J)
            IF(DCHEC2.GT.LOG(EPS).OR.DCHECK.GT.LOG(EPS)) THEN
              JC(IJ+LD+1)=J
              GOTO 200
            END IF
          END DO
  200     CONTINUE
          DO J=JC(IJ+LD)+1,JC(IJ+LD+1)
            DCHECK=LOG(ABS(PM(J,1)))+WP(J)
            DCHEC2=LOG(ABS(PM(J,2)))+WP(J)
            PM(J,1)=SIGN(EXP(DCHECK),PM(J,1))
            PM(J,2)=SIGN(EXP(DCHEC2),PM(J,2))
          END DO
        END DO
        
      END IF

      END
