########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl lvogzs_
.globl _lvogzs_	
lvogzs_:
_lvogzs_:
	movl (%rdi),%edi
	shlq $6,%rdi
	movq %rdi,%r8
	shlq $2,%rdi
	addq %r8,%rdi
	addq %rcx,%rdi

	vsubpd %zmm0,%zmm0,%zmm0
        vsubpd %zmm1,%zmm1,%zmm1
        vsubpd %zmm2,%zmm2,%zmm2
        vsubpd %zmm3,%zmm3,%zmm3

	vbroadcastsd   (%rsi),%zmm8
	vbroadcastsd  8(%rsi),%zmm9
	vbroadcastsd 16(%rsi),%zmm10
	vbroadcastsd 24(%rsi),%zmm11

L00:
	vmovapd   (%rcx),%zmm15
	vmovapd   (%rcx),%zmm14
	vmovapd 64(%rcx),%zmm12
	vfmadd213pd %zmm9,%zmm8,%zmm15
	vmovapd 128(%rcx),%zmm13		
	vfmadd213pd %zmm11,%zmm10,%zmm14	
	
	vfmadd231pd 192(%rcx),%zmm12,%zmm1
	vfmadd231pd 256(%rcx),%zmm12,%zmm0		
	vfmadd231pd 192(%rcx),%zmm13,%zmm3	
	vfmadd231pd 256(%rcx),%zmm13,%zmm2
	
	vfmadd213pd %zmm12,%zmm13,%zmm15
	vmovapd %zmm15,64(%rcx)
	vfmadd213pd %zmm13,%zmm15,%zmm14
	vmovapd %zmm14,128(%rcx)
	
	addq $320,%rcx
	cmpq %rcx,%rdi
	jne L00

	vshuff64x2 $11,%zmm0,%zmm0,%zmm8
	vshuff64x2 $11,%zmm1,%zmm1,%zmm9
	vshuff64x2 $11,%zmm2,%zmm2,%zmm10
	vshuff64x2 $11,%zmm3,%zmm3,%zmm11	
	
	vaddpd %ymm8,%ymm0,%ymm0
	vaddpd %ymm9,%ymm1,%ymm1
	vaddpd %ymm10,%ymm2,%ymm2
	vaddpd %ymm11,%ymm3,%ymm3
	
	vhaddpd %ymm0,%ymm0,%ymm0
	vhaddpd %ymm1,%ymm1,%ymm1
	vhaddpd %ymm2,%ymm2,%ymm2
	vhaddpd %ymm3,%ymm3,%ymm3
	
	vextractf128 $1,%ymm0,%xmm8
	vextractf128 $1,%ymm1,%xmm9
	vextractf128 $1,%ymm2,%xmm10
	vextractf128 $1,%ymm3,%xmm11
	
	vaddsd (%rdx),%xmm8,%xmm8
	vaddsd 8(%rdx),%xmm9,%xmm9
	vaddsd 16(%rdx),%xmm10,%xmm10
	vaddsd 24(%rdx),%xmm11,%xmm11
	
	vaddsd %xmm8,%xmm0,%xmm0
	vaddsd %xmm9,%xmm1,%xmm1
	vaddsd %xmm10,%xmm2,%xmm2
	vaddsd %xmm11,%xmm3,%xmm3
	
	vmovsd %xmm0,(%rdx)
	vmovsd %xmm1,8(%rdx)
	vmovsd %xmm2,16(%rdx)
	vmovsd %xmm3,24(%rdx)

	ret
