%***********************************************************************
% ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
% Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
%
% This library is free software; you can redistribute it and/or
% modify it under the terms of the GNU Lesser General Public
% License as published by the Free Software Foundation; either
% version 2.1 of the License, or (at your option) any later version.
%
% This library is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public
% License along with this library; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
% 02110-1301 USA.
%***********************************************************************
%履歴  2015/10/05 石岡圭一 (version 0.0)
%履歴  2016/03/03 石岡圭一 (version 0.1)
%履歴  2016/12/23 石岡圭一 (version 0.2)
%        LVTSZG, LVTSWG, LVTGZS, LVTGWS のマニュアル中の誤りを修正
%履歴  2017/08/30 石岡圭一 (version 0.3)
%        LVTGZS, LVTSZG のマニュアル中の誤りを修正
%
\documentclass[a4j]{jarticle}

\title{LVPACK使用の手引}
\author{}
\date{}

\begin{document}

\maketitle

\section{概要}

これは, ルジャンドル陪関数変換を行なうサブルーチンパッケージである.

ルジャンドル陪関数の東西波数を$m$, 全波数を$n$とし, 切断波数をそれぞれ
$M$, $N$とする(ただし, $N\ge M$ とする. $N=M$としたときが三角切断
である). このとき, この切断波数におけるルジャンドル陪関数逆変換は, 以下の
ように表せる.
\begin{equation}
G^m(\varphi)\equiv\sum^N_{n=|m|}s^m_nP^m_n(\sin\varphi)
\end{equation}
ここに, $\varphi$は緯度であり, 
$P^m_n(\mu)$は2に正規化されたルジャンドル陪関数で, 以下のように
定義される:
\begin{equation}
P^m_n(\mu)\equiv\sqrt{(2n+1)\frac{(n-|m|)!}{(n+|m|)!}}
\frac1{2^nn!}(1-\mu^2)^{|m|/2}
\frac{d^{n+|m|}}{d\mu^{n+|m|}}(\mu^2-1)^n,
\end{equation}
\begin{equation}
\int^1_{-1}\{P^m_n(\mu)\}^2d\mu=2.
\end{equation}

また, ルジャンドル陪関数正変換は以下のように表せる.
\begin{equation}
s^m_n=\frac12\int^{\pi/2}_{-\pi/2}G^m(\varphi)P^m_n(\sin\varphi)\cos\varphi
d\varphi.
\end{equation}

数値計算においては, 
ルジャンドル正変換の部分は, ガウス-ルジャンドル積分公式により,
\begin{equation}
s^m_n=\frac12\sum^J_{j=1}w_jG^m(\varphi_j)P^m_n(\sin\varphi_j)
\end{equation}
として近似する. ここに, $\varphi_j$はガウス緯度と呼ば
れる分点で, ルジャンドル多項式$P_J(\sin\varphi)$ (ルジャンドル陪関数の
定義式中で$m=0$とし, 正規化係数($\sqrt{\quad}$の部分)を無くしたもの)
の$J$個の零点(を小さい方から順に並べた
もの)であり, $w_j$は各分点に対応するガウシアンウェイトと呼ばれる重みで,
\begin{equation}
w_j\equiv\frac{2(1-\mu_j^2)}{\{JP_{J-1}(\mu_j)\}^2}
\end{equation}
で与えられる. ここに, $\mu_j\equiv\sin\varphi_j$である.
ある条件のもとでは, この積分公式は完全な近似, すなわちもとの積分と同じ
値を与える.

本サブルーチンパッケージは, 
スペクトルデータ($s^m_n$) 
$\to$ 各緯度での波データ($G^m(\varphi_j)$) 
の逆変換を行うルーチン群,
各緯度での波データ($G^m(\varphi_j)$) 
$\to$ スペクトルデータ($s^m_n$) 
の正変換を行うルーチン群,
そして, その他の補助ルーチン群よりなっている.

ここに, 
緯度$\varphi_j$は上述の$J$個のガウス緯度である.
以下のサブルーチンの説明において,
\begin{center}
\begin{tabular}{ll}
\texttt{MM}:& $m$の切断波数$M$\\
\texttt{NN}:& $n$の切断波数$N$\\
\texttt{NM}:& 使いうる$N$の最大値\\
\texttt{JM}:& ガウス緯度の個数$J$\\
\texttt{N}:& 全波数$n$\\
\texttt{M}:& 帯状波数$m$\\
\texttt{J}:& ガウス緯度の番号$j$\\
\end{tabular}
\end{center}
なる対応関係がある.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{サブルーチンのリスト}

\vspace{1em}
\begin{tabular}{ll}
\texttt{LVINIT} & 初期化\\
\texttt{LVTSZG} & スペクトルデータから波データへの変換(帯状成分)\\
\texttt{LVTGZS} & 波データからスペクトルデータへの変換(帯状成分)\\
\texttt{LVTSWG} & スペクトルデータから波データへの変換(波成分)\\
\texttt{LVTGWS} & 波データからスペクトルデータへの変換(波成分)
\end{tabular}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{サブルーチンの説明}

\subsection{LVINIT}

\begin{enumerate}

\item 機能
\texttt{LVPACK}の初期化ルーチン.
\texttt{LVPACK}の他のサブルーチンで使われる配列\texttt{P, R, JC}
の値を初期化する.

\item 定義

\item 呼び出し方法 
    
\texttt{LVINIT(MM,NM,JM,P,R,JC,WP)}
  
\item パラメーターの説明 
    
\begin{tabular}{lll}
\texttt{MM} & \texttt{(I)} & 入力. $m$の切断波数$M$\\
\texttt{NM} & \texttt{(I)} & 入力. $n$の切断波数$N$の使いうる最大値\\
\texttt{JM} & \texttt{(I)} & 入力. 南北格子点数\\
\texttt{P}  & \texttt{(D(JM/2*(2*MM+5)))} 
      & 出力. LVPACKの他のルーチンで用いられる配列\\
\texttt{R}  & \multicolumn{2}{l}{\texttt{(D(((MM+1)*(2*NM-MM-1)+1)/4*3+(2*NM-MM)*(MM+1)/2))}}\\
 &     & 出力. LVPACKの他のルーチンで用いられる配列\\
\texttt{JC}  & \texttt{(I(MM*(2*NM-MM-1)/8+MM))}    & 出力. LVPACKの他のルーチンで用いられる配列\\
\texttt{WP}  & \texttt{(D(JM/2*MM))} 
      & 作業領域.
\end{tabular}

\item 備考

(a) \texttt{MM} は0以上の整数であること.
     
\texttt{JM}は 2以上の偶数であること. さらに, ISPACKのインストール時に
SSE=avx または SSE=fma とした場合は 8の倍数にしておくと高速になる.
また, SSE=avx512 とした場合は 16の倍数にしておくと高速になる.

\texttt{NM} は \texttt{NM} $\ge$ \texttt{MM}を満していなければならない.

(b) \texttt{LVPACK}を使用している間, 配列\texttt{P, R, JC}
の内容を変更してはならない.

(c) \texttt{P(JM/2,5+2*MM)}と宣言されている場合, 
   \texttt{P(J,1)}:  $\sin(\varphi_{J/2+j})$,
   \texttt{P(J,2)}:  $\frac12 w_{J/2+j}$, 
   \texttt{P(J,3)}:  $\cos(\varphi_{J/2+j})$,
   \texttt{P(J,4)}:  $1/\cos(\varphi_{J/2+j})$,
が格納される.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{LVTSZG}

\begin{enumerate}

\item 機能 

スペクトルデータから波データへの変換(帯状成分).

\item 定義

スペクトル逆変換(概要を参照)によりスペクトルデータ($s^0_n$)の帯状
成分から各緯度での波データの帯状成分
($G^0(\varphi_j)$)を求める.

\item 呼び出し方法 

\texttt{LVTSZG(NM,NN,JM,S,G,P,Q,R,WS,IPOW)}
  
\item パラメーターの説明

\begin{tabular}{lll}
\texttt{NM} & \texttt{(I)} & 入力. $n$の切断波数の最大値\\
\texttt{NN} & \texttt{(I)} & 入力. $n$の切断波数
(\texttt{MM}$\le$\texttt{NN}$\le$\texttt{NM}であること)\\
\texttt{JM} & \texttt{(I)} & 入力. 南北格子点数\\
\texttt{S} & \texttt{(D(NN+1))} & 入力. $s^0_n$が格納されている配列\\
\texttt{G} & \texttt{(D(JM))} & 出力. $G^0(\varphi_j)$が格納される配列\\
\texttt{P}  & \texttt{(D(JM/2*(2*MM+5)))} 
      & 入力. LVINITで与えられた配列\\
\texttt{Q} & \texttt{(D(JM/2*5))} & 作業領域\\
\texttt{R}  & \multicolumn{2}{l}{\texttt{(D(((MM+1)*(2*NM-MM-1)+1)/4*3+(2*NM-MM)*(MM+1)/2))}}\\
 &     & 入力. LVINITで与えられた配列\\
\texttt{WS} & \texttt{(D((NN+1))} & 作業領域\\
\texttt{IPOW} & \texttt{(I)} & 入力. 逆変換と同時に作用させる
                      $1/\cos\varphi$の次数. \\
& & 0から2までの整数.
\end{tabular}

\item 備考

(a) \texttt{S(0:NN)}および\texttt{G(JM)}と宣言されている場合, \texttt{S(N)}
には$s^0_n$を格納すること. また
\texttt{G(J)}には  $G^0(\varphi_j)$が格納される.

(b) \texttt{G}の先頭アドレスは, 
ISPACKのインストール時に SSE=avx または SSE=fma とし, かつ
JMを8の倍数とした場合は, 32バイト境界に
合っていなければならない.
また, SSE=avx512 とし, かつ
JMを 16の倍数とした場合は, 64バイト境界に
合っていなければならない.

(c) \texttt{IPOW}$=l$とすると, $G^0(\varphi_j)$の
    かわりに $(\cos\varphi_j)^{-l}G^0(\varphi_j)$ が出力
    される.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{LVTGZS}

\begin{enumerate}

\item 機能 

波データからスペクトルデータへの変換(帯状成分).


\item 定義

ルジャンドル正変換(概要を参照)により各緯度の
波データ($G^0(\varphi_j)$)
からスペクトルデータの帯状成分($s^0_n$)を求める.

\item 呼び出し方法 

\texttt{LVTGZS(NM,NN,JM,S,G,P,Q,R,WS,IPOW)}
  
\item パラメーターの説明(殆んど LVTSZG の項と同じであるので,
異なる部分のみについて記述する).

\begin{tabular}{lll}
\texttt{S} & \texttt{(D((NN+1))} & 出力 $s^0_n$が格納される配列\\
\texttt{G} & \texttt{(D(JM))} & 入力. $G^0(\varphi_j)$が
 格納されている配列\\
\texttt{IPOW} & \texttt{(I)} & 入力. 正変換と同時に作用させる
                      $1/\cos\varphi$の次数. \\
& & 0から2までの整数.
\end{tabular}

\item 備考

(a) \texttt{S(0:NN)}および\texttt{G(JM)}と宣言されている場合, \texttt{S(N)}
には$s^0_n$が格納される. また
\texttt{G(J)}には  $G^0(\varphi_j)$を格納すること.

(b) \texttt{IPOW}$=l$とすると, $G^0(\varphi_j)$の
    かわりに $(\cos\varphi_j)^{-l}G^m(\varphi_j)$ が入力
    になる. 
   
\end{enumerate}

%---------------------------------------------------------------------

\subsection{LVTSWG}

\begin{enumerate}

\item 機能 

スペクトルデータから波データへの変換(波成分).

\item 定義

スペクトル逆変換(概要を参照)によりスペクトルデータ($s^m_n$)
から各緯度での波データ($G^m(\varphi_j)$)を求める.

\item 呼び出し方法 

\texttt{LVTSWG(NM,NN,JM,M,S,G,P,Q,R,JC,WS,IPOW)}
  
\item パラメーターの説明

\begin{tabular}{lll}
\texttt{NM} & \texttt{(I)} & 入力. $n$の切断波数の最大値\\
\texttt{NN} & \texttt{(I)} & 入力. $n$の切断波数
(\texttt{MM}$\le$\texttt{NN}$\le$\texttt{NM}であること)\\
\texttt{JM} & \texttt{(I)} & 入力. 南北格子点数\\
\texttt{M} & \texttt{(I)} & 入力. 帯状波数(\texttt{M}$\ge$ 0 であること)\\
\texttt{S} & \texttt{(D((NN-M+1)*2))} & 入力. $s^m_n$が格納されている配列\\
\texttt{G} & \texttt{(D(JM*2))} & 出力. $G^m(\varphi_j)$が格納される配列\\
\texttt{P}  & \texttt{(D(JM/2*(2*MM+5)))} 
      & 入力. LVINITで与えられた配列\\
\texttt{Q} & \texttt{(D(JM/2*7))} & 作業領域\\
\texttt{R}  & \multicolumn{2}{l}{\texttt{(D(((MM+1)*(2*NM-MM-1)+1)/4*3+(2*NM-MM)*(MM+1)/2))}}\\
 &     & 入力. LVINITで与えられた配列\\
\texttt{JC}  & \texttt{(I(MM*(2*NM-MM-1)/8+MM))}  & 入力. LVINITで与えられた配列\\
\texttt{WS} & \texttt{(D((NN-M+1)*2))} & 作業領域\\
\texttt{IPOW} & \texttt{(I)} & 入力. 逆変換と同時に作用させる
                      $1/\cos\varphi$の次数. \\
& & 0から2までの整数.
\end{tabular}

\item 備考

(a) \texttt{S(2,M:NN)}および\texttt{G(2,JM)}と宣言されている場合, \texttt{S(1,N)},
\texttt{S(2,N)}には$\mbox{Re}(s^m_n)$, $\mbox{Im}(s^m_n)$を格納すること. また
\texttt{G(1,J)}, \texttt{G(2,J)}には  $\mbox{Re}(G^m(\varphi_j))$, 
$\mbox{Im}(G^m(\varphi_j))$が格納される.

(b) \texttt{G}の先頭アドレスは, 
ISPACKのインストール時に SSE=avx または SSE=fma とし, かつ
JMを8の倍数とした場合は, 32バイト境界に
合っていなければならない.
また, SSE=avx512 とし, かつ
JMを 16の倍数とした場合は, 64バイト境界に
合っていなければならない.


(c) \texttt{IPOW}$=l$とすると, $G^m(\varphi_j)$の
    かわりに $(\cos\varphi_j)^{-l}G^m(\varphi_j)$ が出力
    される.

\end{enumerate}


%---------------------------------------------------------------------

\subsection{LVTGWS}

\begin{enumerate}

\item 機能 

波データからスペクトルデータへの変換(波成分).


\item 定義

ルジャンドル正変換(概要を参照)により各緯度の
波データ($G^m(\varphi_j)$)
からスペクトルデータ($s^m_n$)を求める.

\item 呼び出し方法 

\texttt{LVTGWS(NM,NN,JM,M,S,G,P,Q,R,JC,WS,IPOW)}

  
\item パラメーターの説明(殆んど LVTSWG の項と同じであるので,
異なる部分のみについて記述する).

\begin{tabular}{lll}
\texttt{S} & \texttt{(D((NN-M+1)*2))} & 出力. $s^m_n$が格納される配列\\
\texttt{G} & \texttt{(D(JM*2))} & 入力. $G^m(\varphi_j)$が格納されている配列\\
 格納されている配列\\
\texttt{IPOW} & \texttt{(I)} & 入力. 正変換と同時に作用させる
                      $1/\cos\varphi$の次数. \\
& & 0から2までの整数.
\end{tabular}

\item 備考

(a) \texttt{S(2,M:NN)}および\texttt{G(2,JM)}と宣言されている場合, \texttt{S(1,N)},
\texttt{S(2,N)}には$\mbox{Re}(s^m_n)$, $\mbox{Im}(s^m_n)$が格納される. また
\texttt{G(1,J)}, \texttt{G(2,J)}には  $\mbox{Re}(G^m(\varphi_j))$, 
$\mbox{Im}(G^m(\varphi_j))$を格納すること.

(b) \texttt{IPOW}$=l$とすると, $G^m(\varphi_j)$の
    かわりに $(\cos\varphi_j)^{-l}G^m(\varphi_j)$ が入力
    になる. 
   
\end{enumerate}



\end{document}
