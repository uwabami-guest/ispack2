************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
      SUBROUTINE SUTS2G(MM,NM,NN,IM,JM,JV,S,G,IT,T,P,R,JC,WS,W,IPOW)

      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'mpif.h'
      DIMENSION S(*)
      DIMENSION IT(IM/2),T(IM)
      DIMENSION P(JM/2,*)
      DIMENSION R(*)
      DIMENSION JC(*)
      DIMENSION WS((NN+1)*2,*)
      DIMENSION W(*)
      DIMENSION G(*)
!$    INTEGER omp_get_thread_num,omp_get_max_threads

      CALL SUGPRM(JM,JV,JR)

      CALL MPI_COMM_SIZE(MPI_COMM_WORLD,NP,IERR)
      CALL MPI_COMM_RANK(MPI_COMM_WORLD,IP,IERR)

      JI=((JM/JV-1)/NP+1)*JV

      IF(JI*IP.LT.JM) THEN
        JP1=1+JI*IP
        JP2=MIN(JM,JI*(IP+1))
      ELSE ! そのプロセスでは全く緯度円を担当しない場合
        JP1=0
        JP2=-1
      END IF

      NBUF=2*JV*((JM/JV-1)/NP+1)*((IM/2-1)/NP+1)*NP

      NTHMAX=1
!$    NTHMAX=omp_get_max_threads()

      N1=(MM+1)/NP
      M1=N1*NP
      MN=N1
      IF(MOD(N1,2).EQ.0) THEN
        IF(IP.LE.MM-M1) THEN
          MN=MN+1
        END IF
      ELSE
        IF(IP.GE.NP-MM+M1-1) THEN
          MN=MN+1
        END IF
      END IF

      NTH=MIN(MN,NTHMAX)

      ITH=0
!$omp parallel private(K,NS,M,IJ,IE,JS,JE,J,IPDEST,ITH) num_threads(NTH)
!$omp do schedule(dynamic)
      DO K=0,MN-1
!$      ITH=omp_get_thread_num()
        IE=5*K*(2*NM-NP*(K-1))/4+1
        IJ=K*(2*NM-NP*(K-1)+8)/8+1
        M=K*NP+IP+MOD(K,2)*(NP-2*IP-1)
        NS=K*(2*(NN+1)-(K-1)*NP)+1
        IF(M.EQ.0) THEN
          CALL LVSSZG(NM,NN,JM,JV,JR,S,W(ITH*JM*6+1),P,
     &        W(ITH*JM*6+JM*2+1),R,WS(1,ITH+1),IPOW,0)
        ELSE
          CALL LVSSWG(NM,NN,JM,JV,JR,M,S(NS),W(ITH*JM*6+1),P,P(1,6+K*2),
     &        W(ITH*JM*6+JM*2+1),R(IE),JC(IJ),WS(1,ITH+1),IPOW,0)
        END IF
        JS=1
        IPDEST=-1
        DO WHILE(JS.LE.2*JM) 
          IPDEST=IPDEST+1
          JE=MIN(2*JM,2*JI*(IPDEST+1))
          DO J=JS,JE
            W(NBUF*3+(IPDEST*(MM/NP+1)+K)*JI*2+J-JS+1)=W(ITH*JM*6+J)
          END DO
          JS=JE+1
        END DO
      END DO
!$omp end do
!$omp end parallel

      CALL MPI_ALLTOALL(W(NBUF*3+1),JI*2*(MM/NP+1),MPI_REAL8,
     &    W,JI*2*(MM/NP+1),MPI_REAL8,MPI_COMM_WORLD,IERR)

      NTH=MIN((JP2-JP1+1)/JV,NTHMAX)

      ITH=0
      IF(JP1.NE.0) THEN
!$omp parallel private(JD,M,K,I,IPSRC,IV,ITH) num_threads(NTH)
!$omp do schedule(dynamic)
        DO JD=1,(JP2-JP1+1)/JV
!$      ITH=omp_get_thread_num()
          DO M=0,MM
            K=M/NP
            IF(MOD(K,2).EQ.0) THEN
              IPSRC=M-K*NP
            ELSE
              IPSRC=(K+1)*NP-M-1
            END IF
            DO IV=1,JV*2
              W(NBUF+IV+(JV*2)*M+(JV*2*IM/2)*ITH)
     &            =W(IV+2*JV*(JD-1)+JI*2*(K+(MM/NP+1)*IPSRC))
            END DO
          END DO
          DO M=MM+1,IM/2-1
            DO IV=1,JV*2
              W(NBUF+IV+(JV*2)*M+(JV*2*IM/2)*ITH)=0
            END DO
          END DO
          CALL FVRTBA(JV,IM,W(NBUF+1+(JV*2*IM/2)*ITH),IT,T)
          DO IV=1,JV
            DO I=1,IM
              G(I+IM*(IV-1+JV*(JD-1)))=W(NBUF+IV+JV*(I-1)+JV*IM*ITH)
            END DO
          END DO
        END DO
!$omp end do
!$omp end parallel
      END IF

      END
