************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
************************************************************************
*     REPACKING SPECTRA FOR y-div                             20016/02/03
************************************************************************
      SUBROUTINE SUCY2S(MM,SG,S,C)

      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'mpif.h'
      DIMENSION S(*),SG(*),C(*)
!      DIMENSION S((MM/NP+1)*(2*(MM+1)-MM/NP*NP))
!      DIMENSION SG((MM/NP+1)*(2*(MM+2)-MM/NP*NP))
!      DIMENSION C((MM/NP+1)*(2*(MM+1)-MM/NP*NP))

      CALL MPI_COMM_SIZE(MPI_COMM_WORLD,NP,IERR)
      CALL MPI_COMM_RANK(MPI_COMM_WORLD,IP,IERR)

      N1=(MM+1)/NP
      M1=N1*NP
      MN=N1
      IF(MOD(N1,2).EQ.0) THEN
        IF(IP.LE.MM-M1) THEN
          MN=MN+1
        END IF
      ELSE
        IF(IP.GE.NP-MM+M1-1) THEN
          MN=MN+1
        END IF
      END IF

!$omp parallel private(NS,NSG,M)
!$omp do schedule(dynamic)
      DO K=0,MN-1
        M=K*NP+IP+MOD(K,2)*(NP-2*IP-1)
        NS=K*(2*(MM+1)-(K-1)*NP)+1
        NSG=K*(2*(MM+2)-(K-1)*NP)+1
        IF(M.EQ.0) THEN
         CALL SUCYZS(MM,SG,S,C)
        ELSE
          CALL SUCYWS(MM,M,SG(NSG),S(NS),C(NS))
        END IF
      END DO
!$omp end do
!$omp end parallel

      END
