************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
************************************************************************
*     REPACKING SPECTRA TRANSFORM for y div (wave component)  2016/02/03
************************************************************************
      SUBROUTINE SUCYWS(MM,M,ZG,Z,C)

      IMPLICIT REAL*8(A-H,O-Y),COMPLEX*16(Z)
      DIMENSION Z(M:MM),ZG(M:MM+1)
      DIMENSION C(2*(MM-M+1))

      N=M
      Z(N)=-C(N-M+1)*ZG(N+1)
      DO N=M+1,MM
        Z(N)=-C(N-M+1)*ZG(N+1)-C(1+MM-M+N-M)*ZG(N-1)
      END DO

      END
