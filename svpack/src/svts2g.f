************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
      SUBROUTINE SVTS2G(MM,NM,NN,IM,JM,S,G,IT,T,P,R,JC,WS,W,IPOW)

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION S((2*NN+1-MM)*MM+NN+1),G(JM*IM)
      DIMENSION IT(IM/2),T(IM),P(JM/2,2*MM+5),R(*),JC(*)
      DIMENSION WS((NN+1)*2,0:MM),W(JM*IM*2)

!$    INTEGER omp_get_thread_num,omp_get_num_threads,omp_get_max_threads

      NPMAX=1
!$    NPMAX=omp_get_max_threads()
      
      CALL SVGPRM(JM,JV,JR)

      NP=MIN((MM+1)/2,NPMAX)  ! MM≧1 を課しておくことにする.
      IP=0
!$omp parallel private(NS,IP,IJ,IE) num_threads(NP)
!$omp do schedule(dynamic)
      DO M=0,MM
!$      IP=omp_get_thread_num()
        IF(M.EQ.0) THEN
          CALL LVSSZG(NM,NN,JM,JV,JR,S,W,P,W(1+IM*JM+JM/2*7*IP),
     &        R,WS(1,IP),IPOW,0)
        ELSE
          NS=1+NN+1+(M-1)*(NN+NN+2-M)
          IE=(M*(2*NM-M)+1)/4*3+M*(2*NM-M+1)/2+1
          IJ=(M-1)*(2*NM-M)/8+M
          CALL LVSSWG(NM,NN,JM,JV,JR,M,S(NS),W(1+M*2*JM),P,P(1,4+M*2),
     &        W(1+IM*JM+JM/2*7*IP),R(IE),JC(IJ),WS(1,IP),IPOW,0)
        END IF
      END DO
!$omp end do
!$omp end parallel

      NP=MIN(JM/JV,NPMAX)
!$omp parallel private(J,M,IP) num_threads(NP)
!$omp do schedule(dynamic)
      DO JD=1,JM/JV
!$      IP=omp_get_thread_num()
        IF(JV.EQ.4) THEN
          CALL SVQTB1(JM/JV,MM,IM,W(1+2*JV*(JD-1)),
     &      W(IM*JM+1+JV*IM*IP))
        ELSE IF(JV.EQ.8) THEN
          CALL SVOTB1(JM/JV,MM,IM,W(1+2*JV*(JD-1)),
     &      W(IM*JM+1+JV*IM*IP))
        ELSE
          CALL SVLTB1(JV,JM/JV,MM,IM,W(1+2*JV*(JD-1)),
     &      W(IM*JM+1+JV*IM*IP))
        END IF
        CALL FVRTBA(JV,IM,W(IM*JM+1+JV*IM*IP),IT,T)
        IF(JV.EQ.4.AND.IM.GE.16) THEN
          CALL SVQTB2(IM,W(IM*JM+1+JV*IM*IP),G(1+JV*IM*(JD-1)))
        ELSE IF(JV.EQ.8.AND.IM.GE.8) THEN
          CALL SVOTB2(IM,W(IM*JM+1+JV*IM*IP),G(1+JV*IM*(JD-1)))
        ELSE
          CALL SVLTB2(JV,IM,W(IM*JM+1+JV*IM*IP),G(1+JV*IM*(JD-1)))
        END IF
      END DO
!$omp end do
!$omp end parallel

      END
