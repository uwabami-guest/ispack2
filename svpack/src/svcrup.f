************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
************************************************************************
*     REPACKING SPECTRA for larger truncation number NN       2016/02/02
************************************************************************
      SUBROUTINE SVCRUP(MM,NN,S,SR)

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION S((MM+1)*(MM+1)),SR((2*NN+1-MM)*MM+NN+1)

!$omp parallel private(NS,NSR,N,L)
!$omp do schedule(dynamic)
      DO M=0,MM
        IF(M.EQ.0) THEN
          DO N=0,MM
            SR(N+1)=S(N+1)
          END DO
          DO N=MM+1,NN
            SR(N+1)=0
          END DO
        ELSE
          NS=MM+2+(M-1)*(2*MM+2-M)
          NSR=NN+2+(M-1)*(2*NN+2-M)
          DO L=0,2*(MM-M+1)-1
            SR(NSR+L)=S(NS+L)
          END DO
          DO L=2*(MM-M+1),2*(NN-M+1)-1
            SR(NSR+L)=0
          END DO
        END IF
      END DO
!$omp end do
!$omp end parallel

      END
