************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
      SUBROUTINE SVINIT(MM,NM,IM,JM,IT,T,P,R,JC,WP)

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION IT(IM/2),T(IM)
      DIMENSION P(JM/2,5+2*MM),WP(JM/2,MM)
      DIMENSION R(((MM+1)*(2*NM-MM-1)+1)/4*3+(2*NM-MM)*(MM+1)/2)
      DIMENSION JC(MM*(2*NM-MM-1)/8+MM)

      CALL FVRINI(IM,IT,T)
      CALL LVINIT(MM,NM,JM,P,R,JC,WP)

      END
